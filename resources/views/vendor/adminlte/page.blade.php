@extends('adminlte::master')

@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@section('adminlte_css')
    @stack('css')
    @yield('css')
@stop

 

@section('classes_body', $layoutHelper->makeBodyClasses())

@section('body_data', $layoutHelper->makeBodyData())

@section('body')
    <div class="wrapper">

        {{-- Top Navbar --}}
        @if($layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.navbar.navbar-layout-topnav')
        @else
            @include('adminlte::partials.navbar.navbar')
        @endif

        {{-- Left Main Sidebar --}}
        @if(!$layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.sidebar.left-sidebar')
        @endif

        {{-- Content Wrapper --}}
        @empty($iFrameEnabled)
            @include('adminlte::partials.cwrapper.cwrapper-default')
        @else
            @include('adminlte::partials.cwrapper.cwrapper-iframe')
        @endempty

        {{-- Footer --}}
        {{-- @hasSection('footer') --}}
            {{-- @include('adminlte::partials.footer.footer') --}}

        {{-- @endif --}}

        {{-- Right Control Sidebar --}}
        @if(config('adminlte.right_sidebar'))
            @include('adminlte::partials.sidebar.right-sidebar')
        @endif
     @section('js')
            <script type="text/javascript">
                    $(document).ready(function() {

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });


                        function sendMarkRequest(id = null) {
                           
                        
                            console.log(id);
                            
                            $.ajax("{{ route('Admin.notifications.markAsRead') }}", {

                                method: 'POST',
                                data: {
                                    id
                                }

                            }).done(function(res) {
                                console.log(res);
                            });


                        }


                        function deleteRequest(id = null) {
                            $.ajax("{{ route('Admin.notifications.delete') }}", {

                                method: 'POST',
                                data: {
                                    id
                                }

                            }).done(function(res) {
                                console.log(res);
                            });


                        }

                        $('.mark-as-read').click(function() {
                            //  event.preventDefault();
                            let request = sendMarkRequest($(this).data('id'));
                        });

                        $('.delete-notification').click(function() {

                            
                            let request = deleteRequest($(this).data('id'));
                        });
 
                        
 
                    });
            </script>
        @stop
</div>

@stop

@section('adminlte_js')
    @stack('js')
    @yield('js')
@stop

