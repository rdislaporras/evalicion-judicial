@extends('adminlte::page')
@section('css')
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap4.min.css') }}">
@stop


@section('content') 

    <div class="container"style="height:80vh;overflow:hidden">
        <div class=" row d-flex  flex-column justify-content-center align-items-center w-100 h-100">

            <img src="{{ asset('img/logoNew.png') }}" class="border border-success" style="width:180px; background-color:white; border-radius:15%  "> 
            
            <br> 
            <br> 

            

            <h1 class="text-success font-weight-bold " style="font-size:6vw;">
                Atención
            </h1>

            <br> 

            <h2 class=" text-success font-weight-bolder "  style="font-size:3vw;">
                  Acción no Permitida!
            </h2>
        </div>
    </div>

@stop 