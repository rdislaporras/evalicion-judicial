@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('title', 'Remitente')
@section('content_header')

    <div class="card ">

        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> ESTADOS </h1>

            <div class="  ml-auto">
                <div class="btn-group  ">
                    {{-- <a class="btn btn-success" href="{{ route('Admin.states.create') }}">Crear Nuevo</a> --}}

                    <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success">Crear
                        Nuevo</button>
                </div>
            </div>

        </div>

        <div class="card-footer">
            {{ Breadcrumbs::render('states') }}
        </div>


    </div>

@stop




@section('content')

    @include('flash-message')

    <div class="container card p-5">
        <div class="row">
            <div class="   w-100" style="width:100%">

                <div class="card-body">

                    <table id="states" class="table table-striped table-bordered" style="width:100%">
                        <thead class="thead-inverse">
                            <tr>
                                <th>id</th>
                                <th>Nombre</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>

        </div>
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Estado</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="{{ route('Admin.states.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="form-group has-success">
                                {!! Form::label('name', 'Nombre del nuevo estado', ['class' => 'control-label']) !!}
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-success">Crear</button>
                        </div>
                    </form>
                </div>



            </div>
        </div>
    </div>

@stop

@section('js')

    <script src="{{ asset('/js/jquery-3.5.1.js') }}"></script>
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/js/responsive.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#states').DataTable({
                "serverSide": true,
                "ajax": "{{ url('api/estados') }}",
                "columns": [

                    {
                        data: "id"
                    },
                    {
                        data: "name"
                    },

                    {
                        data: 'btn'
                    },

                ],
                order: [
                    [0, 'desc']
                ],
                responsive: true,
                autoWidth: false,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No hay registros para mostrar",
                    "info": "Mostrar página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "search": "Buscar",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)"
                }
            });
        });
    </script>
@stop
