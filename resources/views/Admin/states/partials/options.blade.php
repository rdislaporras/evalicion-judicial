@if ($deleted_at)
<a href="{{ route('Admin.states.restore', $id) }}"><i class="fas fa-recycle text-secondary"></i></a>
<a href="{{ route('Admin.states.forceDelete', $id) }}"><i class="fas fa-fire text-red"></i></a>
@else
<a href="{{ route('Admin.states.edit', $id) }}"><i class="fas fa-pencil-alt text-green"></i></a>
 <a href="{{ route('Admin.states.delete', $id) }}"><i class="fas fa-trash-alt text-red" aria-hidden="true"></i></a>
@endif