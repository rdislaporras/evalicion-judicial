@extends('adminlte::page')



{{-- @section('plugins.Sweetalert2', true) --}}

@section('title', 'states')
@section('content_header')

    <div class="card ">

        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> USUARIOS </h1>

            <div class="  ml-auto">
                <div class="btn-group  ">                
                    <a class="btn btn-warning" href="{{ route('Admin.states.index') }}"><i class="fas fa-reply-all mr-1"></i>Regresar</a>
                    <a class="btn btn-success" href="{{ URL::route('Admin.states.create') }}">Crear Nuevo</a>
                </div>
            </div>

        </div>

        <div class="card-footer">
            {{ Breadcrumbs::render('Admin.states.edit', $data->id) }}
        </div>

 
    </div>

@stop

@section('content')
    <div class="container   p-5"> 
        <div class="row">
            <div class="card  w-100 m-5">

                <div class="card-body ">

                    {!! Form::model($data, ['route' => 'Admin.states.update', $data, 'method' => 'PUT']) !!}

                    @include('Admin.states.partials.form')

                    {!! Form::submit('Actualizar Usuario', ['class' => 'btn btn-primary mt-5']) !!}
                    {!! Form::close() !!}
                </div>

            </div>
        </div>             
    </div>
@stop
