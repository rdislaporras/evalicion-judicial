@extends('adminlte::page')

@section('css')
    {{-- <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap4.min.css') }}">
@stop


{{-- @section('plugins.Sweetalert2', true) --}}

@section('title', 'Users')
@section('content_header')

    <div class="card ">

        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> USUARIOS </h1>

            <div class="  ml-auto">
                <div class="btn-group  ">                
                    <a class="btn btn-warning" href="{{ route('Admin.user.index') }}"><i class="fas fa-reply-all mr-1"></i>Regresar</a>
                    <a class="btn btn-success" href="{{ URL::route('Admin.user.create') }}">Crear Nuevo</a>
                </div>
            </div>

        </div>

        <div class="card-footer">
            {{ Breadcrumbs::render('Admin.user.edit', $data->id) }}
        </div>

 
    </div>

@stop

@section('content')
    <div class="card">
        <div class="container-fluid">
            <div class="row">
                <div class="card  w-100 m-5">

                    <div class="card-body ">

                        {!! Form::model($data, ['route' => 'Admin.user.update', $data, 'method' => 'PUT']) !!}

                        @include('Admin.users.partials.form')

                        {!! Form::submit('Actualizar Usuario', ['class' => 'btn btn-primary mt-5']) !!}
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
