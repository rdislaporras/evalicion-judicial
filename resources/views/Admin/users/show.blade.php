@extends('adminlte::page')

@section('css')
    {{-- <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap4.min.css') }}">
@stop


{{-- @section('plugins.Sweetalert2', true) --}}

@section('title', 'Users')
@section('content_header')

    <div class="card">

        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> Detalles de Usuario: #{{ $data->id }} </h1>


            <div class="  ml-auto">
                <div class="btn-group  ">
                    <a class="btn btn-warning" href="{{ route('Admin.user.index') }}"><i class="fas fa-reply-all mr-1"></i>Regresar</a>
                    <a class="btn btn-info" href="{{ route('Admin.user.edit', $data->id) }}">Editar Usuario</a>
                    <a class="btn btn-success" href="{{ URL::route('Admin.user.create') }}">Crear Nuevo</a>
                </div>
            </div>

        </div>
        <div class="card-footer">
            {{ Breadcrumbs::render('Admin.user.show', $data->id) }}
        </div>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="container">
            <div class="row">
                <div class="col  p-5">

                     {!! Form::model($data, ['route' => 'Admin.user.update', $data, 'method' => 'PUT']) !!}

                        @include('Admin.users.partials.form')

                        <a class="btn btn-primary" href="{{ URL::route('Admin.user.index') }}">Regresar</a>

                    {!! Form::close() !!}
                    
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        $("input").prop("disabled", true);
        $("select").prop("disabled", true);
        $("textarea").prop("disabled", true);
    </script>
@stop