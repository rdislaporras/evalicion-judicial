@extends('adminlte::page')
@section('title', 'Users')
@section('content_header')

    <div class="card">

        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> Nuevo Usuario </h1>

            <div class="  ml-auto">
                <div class="btn-group  ">
                    <a class="btn btn-warning" href="{{ route('Admin.user.index') }}"><i class="fas fa-reply-all mr-1"></i>Regresar</a>
                 </div>
            </div>
        </div>

        <div class="card-footer">
            {{ Breadcrumbs::render('Admin.user.create') }}
        </div>

    </div>
@stop

@section('content')
    <div class="card">
        <div class="container-fluid">
            <div class="row">
                <div class="card w-100 m-5">

                    <div class="card-body ">
                        <form  action="{{ route('Admin.user.store') }}" method="POST" enctype="multipart/form-data">

                        {{-- {!! Form::model(['route' => 'Admin.user.store', 'method' => 'HEAD', 'enctype'=>'multipart/form-data']) !!} --}}
                {{ csrf_field() }}

                        <div class="form-group has-success">
                            {!! Form::label('name', 'Nombre de Usiario', ['class' => 'control-label']) !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group has-success">
                            {!! Form::label('email', 'E-Mail de usuario', ['class' => 'control-label']) !!}
                            {!! Form::email('email', null, ['class' => 'form-control mr-1']) !!}
                        </div>

                        <div class="form-group has-success">
                            {!! Form::label('password', 'Contraseña de Usiario', ['class' => 'control-label']) !!}
                            {!! Form::password('password', ['class' => 'form-control mr-1']) !!}
                        </div>

                        <div class="form-group has-success">
                            {!! Form::label('password', 'Dirección del Usiario', ['class' => 'control-label']) !!}
                            {!! Form::select('direction_id', $direcciones, null, [
                                'class' => 'form-control  ',
                                'placeholder' => 'Por favor seleccione la dirección al que pertenece el usuario',
                            ]) !!}
                        </div>



                        {{-- <div class="container m-5">
                            <div class="row">
                                <div class="d-flex flex-wrap">
                                    @foreach ($roles as $role)
                                        <label class="mr-5" for="">
                                            {!! Form::checkbox($role->id, null, ['class' => 'form-check-input mr-1']) !!}
                                            {{ $role->name }}
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div> --}}

                        {!! Form::submit('Crear Usuario', ['class' => 'btn btn-primary mt-5']) !!}
                        {{-- {!! Form::close() !!} --}}
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
@stop