@extends('adminlte::page')

@section('css')
    {{-- <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap4.min.css') }}">
@stop



{{-- @section('plugins.Sweetalert2', true) --}}


@section('title', 'Usuarios')
@section('content_header')

    <div class="card ">

        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> USUARIOS </h1>

            <div class="  ml-auto">
                <div class="btn-group  "> 
                    <a class="btn btn-success" href="{{ URL::route('Admin.user.create') }}">Crear Nuevo</a>
                </div>
            </div>

        </div>

        <div class="card-footer">
            {{ Breadcrumbs::render('usuarios') }}
        </div>

 
    </div>

@stop




@section('content')

    @include('flash-message')
 
    <div class="container-fluid">
        <div class="row">
            <div class="card  w-100" style="width:100%">
                
                <div class="card-body">

                    <table id="usuarios" class="table table-striped table-bordered" style="width:100%">
                        <thead class="thead-inverse">
                            <tr>
                                <th>id</th>
                                <th>Nombre</th>
                                <th>Email</th> 
                                <th>F-Creación</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>

        </div>
    </div>
 
@stop

@section('js')

    <script src="{{ asset('/js/jquery-3.5.1.js') }}"></script>
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/js/responsive.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#usuarios').DataTable({
                "serverSide": true,
                "ajax": "{{ url('api/usuarios') }}",
                "columns": [{
                        data: "id"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "email"
                    }, 
                    {
                        data: "created_at"
                    },
                    {
                        data: 'btn'
                    },
                ],
                responsive: true,
                autoWidth: false,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No hay registros para mostrar",
                    "info": "Mostrar página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "search": "Buscar  ",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)"
                }
            });
        });
    </script>




@stop
