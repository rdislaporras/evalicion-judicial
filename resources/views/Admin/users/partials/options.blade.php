<a href="{{ route('Admin.user.edit',$id) }}"><i class="fas fa-pencil-alt text-green"></i></a>
<a href="{{ route('Admin.user.show',$id) }}"><i class="fas fa-search" aria-hidden="true"></i></a> 
 <a href="{{ route('Admin.user.delete',$id) }}"><i class="fas fa-trash-alt text-red" aria-hidden="true"></i></a>  


{{-- FUNCIONES PARA ADMINISTRADOR    --}}
<a   href="{{ route('Admin.user.restore',$id) }}"><i class="fas fa-recycle text-secondary"></i></a>  
<a   href="{{ route('Admin.user.forceDelete',$id) }}"><i class="fas fa-fire text-red"></i></a>  
 
 