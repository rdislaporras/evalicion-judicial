{!! Form::hidden('id', $data->id) !!}

<div class="form-group has-success">
    <label class="control-label" for="cardExpiry"><b>Nombre de Usuario</b></label>
    {!! Form::text('name', $data->name, ['class' => 'form-control']) !!}
</div>

<div class="form-group has-success">
    <label class="control-label" for="cardExpiry"><b>Correo de Usuario</b></label>
    {!! Form::email('email', $data->email, ['class' => 'form-control mr-1']) !!}
</div>

<div class="form-group has-success">
    <label class="control-label" for="cardExpiry"><b>Contraseña de Usuario</b></label>
    {!! Form::password('password', ['class' => 'form-control mr-1']) !!}
</div>

<div class="form-group has-success">
    {!! Form::label('direction_id', 'Dirección del Usuario', ['class' => 'control-label']) !!}
    {!! Form::select('direction_id', $direcciones, null, [
        'class' => 'form-control  ',
        'placeholder' => 'Por favor seleccione la dirección al que pertenece el usuario',
    ]) !!}
</div>

<div class="container m-5">
    <div class="row">
        <div class="d-flex flex-wrap">
            @foreach ($roles as $role)
                <label class="mr-5" for="">
                    {!! Form::checkbox('roles[]', $role->id, null, ['class' => 'form-check-input mr-1']) !!}
                    {{ $role->name }}
                </label>
            @endforeach
        </div>
    </div>
</div>
