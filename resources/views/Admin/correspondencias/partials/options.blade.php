@if ($deleted_at)

         <a href="{{ route('Admin.correspondence.restore', $id) }}"><i class="fas fa-recycle text-secondary"></i></a>
     
        <a href="{{ route('Admin.correspondence.forceDelete', $id) }}"><i class="fas fa-fire text-red"></i></a>
 

@else
    @can('Admin.correspondence.edit')
        <a class="ml-2 mr-2" href="{{ route('Admin.correspondence.edit', $id) }}"><i class="fas fa-pencil-alt text-green"></i></a>
    @endcan

    <a class="ml-2 mr-2"  href="{{ route('Admin.correspondence.show', $id) }}"><i class="fas fa-search" aria-hidden="true"></i></a>
 

    <a class="ml-2 mr-2"  href="{{ route('Admin.correspondence.pdf', $id) }}" target="_blank" formtarget="_blank" ><i class="far fa-file-pdf text-purple"></i></a>    
{{--  
    <a class="ml-2 mr-2"  href="{{ route('Admin.correspondence.attachs', $id) }}" target="_blank" formtarget="_blank" > 
        <i class="fas fa-cloud-download-alt"></i>
    </a> --}}
  

     
    @can('Admin.correspondence.delete')
        <a href="{{ route('Admin.correspondence.delete', $id) }}"><i class="fas fa-trash-alt text-red" aria-hidden="true"></i></a>
    @endcan

@endif
