<div class="container">
    <div class="row">

        <div class="w-100 d-flex justify-content-around">
            <div class="">
                <label for="email" class="form-label text-center  ">Prefijo:</label>
                <strong>
                    <b>
                        <div class="form-control input-sm btn btn-info" >
                            <span class="badge badge-light">
                                {{ $data->prefijo }}
                            </span>
                        </div>
                    </b>
                </strong>
            </div>
            <div class=" ">
                <div class="form-group has-success">
                    <label class="control-label" for="cardExpiry"><b>N° de Regstr.</b></label>
                    <strong>
                        <b>
                            <div class="form-control input-sm btn btn-success"
                                style="background-color: #059856">
                                <span class="badge badge-light">
                                    {{$data->id}}
                                </span>
                            </div>
                        </b>
                    </strong>
                </div>
            </div>
            <div class=" ">
                <div class="form-group has-success">
                    <label class="control-label" for="cardExpiry"><b>N° de Control.</b></label>
                    <strong>
                        <b>
                            <div class="form-control input-sm btn btn-primary" >
                                <span class="badge badge-light">
                                    {{$data->n_control}}
                                </span>
                            </div>
                        </b>
                    </strong>
                </div>
            </div>
            <div class="">
                <div class="form-group has-success">
                    <label class="control-label" for="cardExpiry"><b>Usuario Autor.</b></label>
                    <strong>
                        <b>
                            <div class="form-control input-sm btn badge-warning"
                                style="background-color:  ">
                                <span class=" text-bold">
                                    {{Auth::user()->name }}
                                </span>
                            </div>
                        </b>
                    </strong>
                </div>
            </div>
        </div>

    </div>
</div>


<hr>


<div class="row">

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip"
        title="Fecha en la que se recibió la presente correspondencia">
        <div class="form-group has-success">
            <label class="control-label" for="text-input"><b>Fecha Recibo</b> </label>
            <input class="form-control input-sm" type="date" name="f_recibo" value="{{$data->f_recibo}}" required 
             {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip" data-placement="top"
        title="Este campo debe ser llenado con números unicamente">
        <div class="form-group has-success">
            <label class="control-label" for="text-input"><b>N° de Comnc.</b></label>
            <input class="form-control input-sm" type="number" name="n_comunc"
                Placeholder="N° de Comunicación (solo se permiten numeros)" data-toggle="tooltip"
                data-placement="top" title="Este campo debe ser llenado con números unicamente"
                 value="{{$data->n_comunc}}"
                 {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}
                >
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center" data-toggle="tooltip"
        title="Presione si desea archivar la presente correspondencia">
        <div class="form-group has-success">
            <label class="control-label" for="cardExpiry"><b>Archivar?</b> </label>
        </div>
        <div class="checkbox">
            <label> 
                <input type="checkbox" name="archivar" id="archivar" value="{{$data->archivar}}"  {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}> 
                Archivar
            </label>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip"
        title="Hora en la que se recibió la presente correspondencia">


        <div class="form-group has-success">
            <label class="control-label" for="text-input"><b>Hora de Recibido.</b></label>
            <input class="form-control input-sm" type="time" name="h_recibo" value="{{$data->h_recibo}}" required  {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}>
        </div>
    </div>


    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip"
        title="Fecha en que se realizó la comunicación de la presente correspondencia">
        <div class="form-group has-success">
            <label class="control-label" for="text-input"><b>Fecha Comnc.</b></label>
            <input class="form-control input-sm " type="date" name="f_comunc" value="{{$data->f_comunc}}" required  {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip"
        title="Indique la fecha en la que será archivada la correspondencia">
        <div class="form-group has-success">
            <label class="control-label" for="cardExpiry"><b>Fecha Archivado</b></label>
            <input class="form-control input-sm" type="date" name="f_archivd" id="f_archivd" value="{{$data->f_archivd}}" disabled  {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}>
        </div>
    </div>

    <div class="col-xs-12 col-md-6 mt-3 ">

        <div class="col" data-toggle="tooltip"
            title="Preseione en la unidad que desea implicar en la presente correspondencia">
            <div class="form-group has-success">
                <label class="control-label" for="cardExpiry"><b>Tipología</b> </label>

                <select name="tipology_id" id="tipologia" class="form-control custom-select"  {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}>
                    <option value="" >Seleccione un tipo de documento</option>
                    @foreach ($tipologias as $item)
                        <option value="{{ $item->id }}"  {{ $item->id == $sel_typology->id ? 'selected' : '' }}>
                            {{ $item->name }}
                        </option>
                    @endforeach
                </select>

            </div>

        </div>
    </div>


</div>


<hr class="mt-5">
<hr class="mb-5">

<div class="row">
    <div class="col-xs-12 col-md-6 ">

        <div class=" col text-center" data-toggle="tooltip" title="Presione el remitente que desea asociar en la presente correspondencia">
            
            <label class="control-label  " for="cardExpiry"><b>Remitentes</b> </label>

            <select name="remitter_id" id="remitentes" class="form-control custom-select"  {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}>
                <option value="" >Seleccione un remitente</option>
                @foreach ($remitentes as $item)
                    <option value="{{ $item->id }}"  {{ $item->id == $sel_remitter->id ? 'selected' : '' }}>
                        {{ $item->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

 
    <div class="col-xs-12 col-md-6 ">

        <div class="col text-center" data-toggle="tooltip"
            title="Seleccione la unidad que desea asociar en la presente correspondencia">
            <div class="form-group has-success">
                <label class="control-label" for="cardExpiry"><b>Gerencias</b> </label>

                <select name="direction_id" id="directions" class="form-control custom-select "  {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}>
                    <option value="" >Seleccione una gerencia</option>
                    @foreach ($direcciones as $item)
                        <option value="{{ $item->id }}"  {{ $item->id == $sel_direction->id ? 'selected' : '' }}>
                            {{ $item->name }}
                        </option>
                    @endforeach
                </select>      

             </div> 
        </div>
    </div>

</div>


<hr class="mt-5">
<hr class="mb-5">

<div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" data-toggle="tooltip"
        title="Indique el asunto de la presente correspondencia">
        <div class="form-group has-success">
            <label class="control-label" for="cardExpiry"><span class="hidden-xs">
                </span><b>Asunto</b></label>
            <textarea class="form-control  input-lg" name="asunto" id="asunto"  
                placeholder="Ingrese en este campo el asunto de la correspondencia" autofocus=""
                onkeyup="javascript:this.value=this.value.toUpperCase();"
                 {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}
                
                >
                {{$data->asunto}}
            </textarea>
        </div>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" data-toggle="tooltip"
        title="Indique la acción de la presente correspondencia">
        <div class="form-group has-success">
            <label class="control-label" for="cardExpiry"><b>Acción</b> </label>
            <textarea class="form-control  input-lg" name="accion" id="accion"
                placeholder="Ingrese en este campo la acción que debe seguir la correspondencia" autofocus=""
                onkeyup="javascript:this.value=this.value.toUpperCase();" 
                 {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}> {{$data->accion}}</textarea>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-toggle="tooltip"
        title="Indique la ubicación de la presente correspondencia">
        <div class="form-group has-success">
            <label class="control-label" for="text-input"><b>Ubicación</b> </label>
            <input type="text" class="form-control input-lg" name="ubicacion" id="ubicacion"
                placeholder="Ingrese la ubicación de la presente correspondencia"
                onkeyup="javascript:this.value=this.value.toUpperCase();"  value="{{$data->ubicacion}}"
                 {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}}>
        </div>
    </div>

</div>


<hr class="mt-5">
<hr class="mb-5">

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-toggle="tooltip"
        title="Indique las observaciones de la presente correspondencia">
        <div class="form-group has-success">
            <label class="control-label" for="text-input"><b>Observaciones</b> </label>
            <textarea class="form-control  input-lg" name=" observaciones" id="observaciones"
                placeholder="Ingrese las observaciones de la presente correspondencia"
                onkeyup="javascript:this.value=this.value.toUpperCase();"   {{ Auth::user()->roles[0]->id == 4 ? "readonly":""}} >{{$data->observaciones}}</textarea>
        </div>
    </div>
</div>

<hr class="mt-5">
<hr class="mb-5">

<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip" title="Seleccione el estado de la correspondencia">
        <div class="form-group has-success">
            {!! Form::label('states', 'Estados del a correspondencia', ['class' => 'control-label']) !!}
            {!! Form::select('state_id', $states, $data->state_id, [ 'class' => 'form-control  ',  'placeholder' => 'Seleccione el estado de la correspondencia', 'id'=>'1' ]) !!}
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" data-toggle="tooltip" title="Indique las observaciones con relación al estado de la correspondencia">
        {!! Form::label('history', 'Historial de estados', ['class' => 'control-label']) !!}
        {!! Form::textarea('history', null, ['class' => 'form-control w-100','rows'=>'5','placeholder' =>'Indique las observaciones con relación al estado de la correspondencia' ]) !!}
    </div>
</div>
@if(!$show)
<hr class="mt-5">
<hr class="mb-5">
 

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-toggle="tooltip"
            title="Adjunte documentación ">
            <div class="form-group has-success">
                <label class="control-label" for="text-input"><b>Documentación Adjunta</b> </label>
                <input type="file" class="form-control input-lg  border-0   " name="attachtment" id="attachtment"
                    placeholder="Adjunte documentación de la correspondencia" >
            </div>
        </div>
    </div>
 @endif

<br>
<br>
<br>










    