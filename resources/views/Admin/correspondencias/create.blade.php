@extends('adminlte::page')



{{-- @section('plugins.Sweetalert2', true) --}}

@section('title', 'Users')
@section('content_header')

    <div class="card">

        <div class="card-body">
            <h1> Nueva Correspondencia </h1>
        </div>

        <div class="card-footer">
            {{ Breadcrumbs::render('Admin.correspondencia.create') }}
        </div>

    </div>
@stop

@section('content')
    <div class="container card p-5">
        <div class="row">
            <div class="col">
                <form class="" action="{{ route('Admin.correspondence.store') }}" method="post"
                    enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="newReg" id="newReg" value="{{ $newReg }}">
                    <input type="hidden" name="prefijo" id="prefijo" value="{{ $prefix }}">
                    <input type="hidden" name="n_control" id="n_control" value=" {{ $n_control }}">
                    <input type="hidden" name="id_userDirection" id="id_userDirection" value=" {{ $userdirection_id }}">
                    <input type="hidden" name="author_id" id="author_id" value="{{ $userId }}" readonly>

                    <div class="container">
                        <div class="row align-content-center"> 

                                <div class="col-xs-12 col-sm-2 col-md-3 col-lg-2">
                                    <label for="email" class="form-label text-center  ">Prefijo:</label>
                                    <strong>
                                        <b>
                                            <div class="form-control input-sm btn btn-info">
                                                <span class="badge badge-light">
                                                    {{ $prefix }}
                                                </span>
                                            </div>
                                        </b>
                                    </strong>
                                </div>

                                <div class="col-xs-12 col-sm-2 col-md-3 col-lg-2 ">
                                    <div class="form-group has-success">
                                        <label class="control-label" for="cardExpiry"><b>N° de Regstr.</b></label>
                                        <strong>
                                            <b>
                                                <div class="form-control input-sm btn btn-success"
                                                    style="background-color: #059856">
                                                    <span class="badge badge-light">
                                                        {{ $newReg }}
                                                    </span>
                                                </div>
                                            </b>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-2 col-md-3 col-lg-2 ">
                                    <div class="form-group has-success">
                                        <label class="control-label" for="cardExpiry"><b>N° de Control.</b></label>
                                        <strong>
                                            <b>
                                                <div class="form-control input-sm btn btn-primary">
                                                    <span class="badge badge-light">
                                                        {{ $n_control }}
                                                    </span>
                                                </div>
                                            </b>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-2 col-md-3 col-lg-2">
                                    <div class="form-group has-success">
                                        <label class="control-label" for="cardExpiry"><b>Usuario Autor.</b></label>
                                        <strong>
                                            <b>
                                                <div class="form-control input-sm btn badge-warning"
                                                    style="background-color:  ">
                                                    <span class=" text-bold">
                                                        {{ $userName }}
                                                    </span>
                                                </div>
                                            </b>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="form-label text-center  ">Dirección del Author:</label>

                                    <div class="form-control input-sm btn btn-info text-white">
                                        <strong>
                                            {{ $UserDirectionName->name }}
                                        </strong>

                                    </div>
                                    </b>
                                </div>
                             

                        </div>
                    </div>


                    
                        <hr class="m-5">


                    <div class="row">

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip"
                            title="Fecha en la que se recibió la presente correspondencia">
                            <div class="form-group has-success">
                                <label class="control-label" for="text-input"><b>Fecha Recibo</b> </label>
                                <input class="form-control input-sm" type="date" name="f_recibo" required>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip" data-placement="top"
                            title="Este campo debe ser llenado con números unicamente">
                            <div class="form-group has-success">
                                <label class="control-label" for="text-input"><b>N° de Comnc.</b></label>
                                <input class="form-control input-sm" type="number" name="n_comunc"
                                    Placeholder="N° de Comunicación (solo se permiten numeros)" data-toggle="tooltip"
                                    data-placement="top" title="Este campo debe ser llenado con números unicamente"
                                    onkeyup="validarSiNumero(this.value);" required>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center" data-toggle="tooltip"
                            title="Presione si desea archivar la presente correspondencia">
                            <div class="form-group has-success">
                                <label class="control-label" for="cardExpiry"><b>Archivar?</b> </label>
                            </div>
                            <div class="checkbox">
                                <label> <input type="checkbox" name="archivar" id="archivar"> Archivar</label>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip"
                            title="Hora en la que se recibió la presente correspondencia">


                            <div class="form-group has-success">
                                <label class="control-label" for="text-input"><b>Hora de Recibido.</b></label>
                                <input class="form-control input-sm" type="time" name="h_recibo" required>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip"
                            title="Fecha en que se realizó la comunicación de la presente correspondencia">
                            <div class="form-group has-success">
                                <label class="control-label" for="text-input"><b>Fecha Comnc.</b></label>
                                <input class="form-control input-sm " type="date" name="f_comunc" required>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-toggle="tooltip"
                            title="Indique la fecha en la que será archivada la correspondencia">
                            <div class="form-group has-success">
                                <label class="control-label" for="cardExpiry"><b>Fecha Archivado</b></label>
                                <input class="form-control input-sm" type="date" name="f_archivd" id="f_archivd"
                                    disabled>
                            </div>
                        </div>

                       

                        <div class="col-xs-12 col-md-6 ">

                            <div class="col" data-toggle="tooltip"
                                title="Preseione en la unidad que desea implicar en la presente correspondencia">
                                <div class="form-group has-success">
                                    <label class="control-label" for="cardExpiry"><b>Tipología</b> </label>

                                    <select name="tipology_id" id="tipologias" class="form-control custom-select">
                                        <option value="">Seleccione un tipo de documento</option>
                                        @foreach ($tipologias as $item)
                                            <option value="{{ $item->id }}">
                                                {{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>

                                </div>

                            </div>
                        </div>

                    </div>

                    
                        <hr class="m-5">




                    

                    <div class="row">
                        <div class="col-xs-12  col-md-6">

                            <div class="col" data-toggle="tooltip"
                                title="Preseione el remitente que desea asosiciar en la presente correspondencia">
                                <div class="form-group has-success">
                                    <label class="control-label" for="cardExpiry"><b>Remitentes</b> </label>

                                    <select name="remitter_id" id="remitentes" class="form-control custom-select">
                                        <option value="">Seleccione un remitente</option>
                                        @foreach ($remitentes as $item)
                                            <option value="{{ $item->id }}">
                                                {{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>

                                </div>


                            </div>
                        </div>


                        <div class="col-xs-12 col-md-6 ">

                            <div class="col" data-toggle="tooltip" tle="Presione en la gerencia que desea seleccionar">
                                <div class="form-group has-success">
                                    
                                    <label class="control-label" for="cardExpiry"><b>Gerencias</b> </label>

                                    <select name="direction_id" id="directions" class="form-control custom-select">
                                        <option value="">Seleccione una gerencia</option>
                                        <option value="">Seleccione una gerencia</option>
                                        @foreach ($direcciones as $item)
                                            <option value="{{ $item->id }}">
                                                {{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>

                                </div>

                            </div>
                        </div>

                    </div>

 

                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" data-toggle="tooltip"
                            title="Indique el asunto de la presente correspondencia">
                            <div class="form-group has-success">
                                <label class="control-label" for="cardExpiry"><span class="hidden-xs">
                                    </span><b>Asunto</b></label>
                                <textarea class="form-control  input-lg" name="asunto" id="asunto"
                                    placeholder="Ingrese en este campo el asunto de la correspondencia" autofocus=""
                                    onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" data-toggle="tooltip"
                            title="Indique la acción de la presente correspondencia">
                            <div class="form-group has-success">
                                <label class="control-label" for="cardExpiry"><b>Acción</b> </label>
                                <textarea class="form-control  input-lg" name="accion" id="accion"
                                    placeholder="Ingrese en este campo la acción que debe seguir la correspondencia" autofocus=""
                                    onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-toggle="tooltip"
                            title="Indique la ubicación de la presente correspondencia">
                            <div class="form-group has-success">
                                <label class="control-label" for="text-input"><b>Ubicación</b> </label>
                                <input type="text" class="form-control input-lg" name="ubicacion" id="ubicacion"
                                    placeholder="Ingrese la ubicación de la presente correspondencia"
                                    onkeyup="javascript:this.value=this.value.toUpperCase();">
                            </div>
                        </div>

                    </div>

 
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-toggle="tooltip"
                            title="Indique las observaciones de la presente correspondencia">
                            <div class="form-group has-success">
                                <label class="control-label" for="text-input"><b>Instrucciones / Observaciones</b> </label>
                                <textarea class="form-control  input-lg" name=" observaciones" id="observaciones"
                                    placeholder="Ingrese las observaciones de la presente correspondencia"
                                    onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
                            </div>
                        </div>
                    </div>

            
                        <hr class="m-5">
 
                    <div class="row">
                  
                            <div class="col-xs-12  col-lg-5    " data-toggle="tooltip"
                                title="Seleccione el estado de la correspondencia">
                                <div class="form-group has-success">
                                    {!! Form::label('states', 'Estados del a correspondencia', ['class' => 'control-label']) !!}
                                    {!! Form::select('state_id', $states, 1, [
                                        'class' => 'form-control  ',
                                        'placeholder' => 'Seleccione el estado de la correspondencia',
                                        'id' => '1',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-xs-12   col-lg-7  " data-toggle="tooltip"
                                title="Indique las observaciones con relación al estado de la correspondencia">
                                {!! Form::label('history', 'Historial de estados', ['class' => 'control-label']) !!}
                                {!! Form::textarea('history', null, [
                                    'class' => 'form-control',
                                    'rows' => '5',
                                    'placeholder' => 'Indique las observaciones con relación al estado de la correspondencia',
                                ]) !!}
                            </div>
                         
                        
                    </div>


                        <hr class="m-5">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-toggle="tooltip"
                            title="Adjunte documentación ">
                            <div class="form-group has-success">
                                <label class="control-label" for="text-input"><b>Documentación Adjunta</b> </label>
                                <input type="file" class="form-control input-lg  border-0   " name="attachtment" id="attachtment"
                                    placeholder="Adjunte documentación de la correspondencia" >
                            </div>
                        </div>
                    </div>

                    <br class=" ">
                    <br class=" ">

                    <div class="row d-flex justify-content-center items-center mt-5">
                         <button type="submit" class="btn btn-primary">Crear Correspondencia </button>
                    </div>

                    
                </form>
            </div>
        </div>
    </div>
@stop


@section('js')

    <script>
        $('#archivar').on('click', function() {
            if ($('#f_archivd').prop('disabled') == false) {
                $('#f_archivd').prop('disabled', true);
            } else {
                $('#f_archivd').prop('disabled', false);
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#directions').select2({
                width: '100%',
                placeholder: "Seleccione una dirección",
                allowClear: true

            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#remitentes').select2({
                width: '100%',
                placeholder: "Seleccione un remitente",
                allowClear: true

            });
        });
    </script>
    <script>
        $(document).ready(function(){
             $(window).scrollTop(0);
        });
    </script>


@stop
