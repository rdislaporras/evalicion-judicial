@extends('adminlte::page')



{{-- @section('plugins.Sweetalert2', true) --}}

@section('title', 'Users')
@section('content_header')

    <div class="card">

        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> Mostrar Correspondencia </h1>


            <div class="  ml-auto">
                <div class="btn-group  ">
                    <a class="btn btn-success" href="{{ route('Admin.correspondence.edit', $data->id) }}">Editar
                        Correspondencia</a>
                    {{-- <a class="btn btn-warning" href="{{ route('correspondencias.restore') }}"> Restaurar <i class="fas fa-recycle"></i></a> --}}
                </div>
            </div>

        </div>
        <div class="card-footer">
            {{ Breadcrumbs::render('Admin.correspondencia.show', $data->id) }}
        </div>
    </div>
@stop

@section('content')
    <div class="container card p-5 ">
             <div class="row">
                <div class="col  p-5">

                    @include('Admin.correspondencias.partials.form')

                    <div class="col d-flex justify-content-center">

                        <a target="_blank" class="btn btn-danger mr-2"
                            href="{{ URL::route('Admin.correspondence.pdf', $data->id) }}">Crear PDF <i
                                class="fas fa-file-pdf text-white "></i></a>

                        <a class="btn btn-info" href="{{ route('Admin.correspondence.attachs', $data->id) }}" target="_blank">Descargar Documentos  <i class="fas fa-download ml-3"></i></a>
                        
                            <a class="btn btn-warning ml-2" href="{{ route('Admin.correspondence.index') }}"><i class="fas fa-reply-all mr-1"></i>Regresar</a>  
                        
                    </div>
                </div>
            </div>
     </div>
@stop

@section('css')
@section('js')
    <script>
        $("input").prop("disabled", true);
        $("select").prop("disabled", true);
        $("textarea").prop("disabled", true);
    </script>
@stop
@section('js')
    <script>
     window.onbeforeunload = function () {
        window.scrollTo(0, 0);
    }
    </script>
@stop
