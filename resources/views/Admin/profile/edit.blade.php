@extends('adminlte::page')



{{-- @section('plugins.Sweetalert2', true) --}}

@section('title', 'Users')
@section('content_header')

    <div class="card ">

        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> Usuario -- {{ Auth::user()->name }} </h1>
        </div>

        <div class="card-footer">
            {{ Breadcrumbs::render('Admin.profile.edit', Auth::user()->name) }}
        </div>


    </div>

@stop

@section('content')

    @include('flash-message')

  
     
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="card d-flex justify-content-center   p-5 ">

                    <div class="card-body  d-flex   justify-content-center  flex-column   align-self-center ">


                        <div class="container mt-5">

                            <div class="row">

                                


                                    {!! Form::model($data, [
                                        'route' => 'Admin.profile.update',
                                        $data,
                                        'method' => 'POST',
                                        'class' => 'w-100 d-flex flex-column   justify-content-center  flex-column   align-self-center',
                                        'enctype' => 'multipart/form-data'
                                    ]) !!}

                                    @include('Admin.profile.partials.form')

                                    {!! Form::submit('Actualizar Usuario', ['class' => 'btn btn-primary mt-5 align-self-center']) !!}
                                    {!! Form::close() !!}
 
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    </script>

@stop
