@extends('adminlte::page')



{{-- @section('plugins.Sweetalert2', true) --}}

@section('title', 'Users')
@section('content_header')

    <div class="card ">

        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> Usuario -- {{ Auth::user()->name }} </h1>
        </div>

        <div class="card-footer">
            {{ Breadcrumbs::render('Admin.profile.show', Auth::user()->name) }}
        </div>


    </div>

@stop

@section('content')

    @include('flash-message') 

     
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="card d-flex justify-content-center   p-5 ">

                    <div class="card-body  col d-flex   justify-content-center  flex-column   align-self-center ">


 
                                    <div class="form-group has-success">
                                        <label class="control-label" for="cardExpiry"><b>Nombre de Usuario</b></label>
                                        <div class="form-control">
                                            {{$data->name}}
                                        </div>
                                         
                                    </div>

                                    <div class="form-group has-success">
                                        <label class="control-label" for="cardExpiry"><b>Correo de Usuario</b></label>
                                         <div class="form-control">
                                            {{$data->email}}
                                        </div>
                                     </div> 

                             
                                                               
                                
                                

                                

                                    {{-- <div class="card p-3 form-group has-success d-flex flex-column justify-content-center">
                                        <div class="card-header">
                                            <label class="control-label" for="cardExpiry"><b>Avatar </b></label>
                                        </div>
                                        <img src="{{ asset('storage/'.$data->avatar) }}" alt="{{ $data->name }}">
                                    </div> --}}

                                 
                            </div>
                            <a class="btn btn-info align-self-center mt-5"
                            href="{{ route('Admin.profile.edit', Auth::id()) }}"><i class="fas fa-pencil-alt mr-1  "></i>
                            Editar Perfil</a>

                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
 
@stop
@section('js')
    <script>
        $("input").prop("disabled", true);
        $("select").prop("disabled", true);
        $("textarea").prop("disabled", true);
    </script>
@stop