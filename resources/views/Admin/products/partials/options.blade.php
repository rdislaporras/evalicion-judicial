@if ($deleted_at)
    <a href="{{ route('Admin.products.restore', $id) }}"><i class="fas fa-recycle text-secondary"></i></a>
    <a href="{{ route('Admin.products.forceDelete', $id) }}"><i class="fas fa-fire text-red"></i></a>
 
@endif

     {{-- @can('Admin.products.edit') --}}
        <a href="{{ route('Admin.products.edit', $id) }}"><i class="fas fa-pencil-alt text-green"></i></a>
  
        <a href="{{ route('Admin.products.delete', $id) }}"><i class="fas fa-trash text-red" aria-hidden="true"></i></a>
    {{-- @endcan --}}