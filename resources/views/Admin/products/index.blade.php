@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('title', 'Dirección')

@section('content_header')
    <div class="card ">
        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> Lista de Productos </h1>

            @can('Admin.products.create')
                <div class="  ml-auto">
                    <div class="btn-group  ">
                         <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success">Crear Nuevo Producto</button>
                    </div>
                </div>
            @endcan


        </div>
 
    </div>
@stop

@section('content')

    @include('flash-message')

    <div class="container card p-5">
        <div class="row">
            <div class="   w-100" style="width:100%">

                <div class="card-body">

                    <table id="direcciones" class="table table-striped table-bordered" style="width:100%">
                        <thead class="thead-inverse">
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Inpuesto</th>
                                <th>Cantidad</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>

        </div>
    </div>



    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Producto</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="{{ route('Admin.products.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="form-group has-success">
                                {!! Form::label('name', 'Nombre del producto', ['class' => 'control-label']) !!}
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-group has-success">
                                {!! Form::label('price', 'Precio del producto', ['class' => 'control-label']) !!}
                                {!! Form::text('price', null, ['class' => 'form-control']) !!}
                            </div>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-group has-success">
                                {!! Form::label('tax', 'Impuesto del producto', ['class' => 'control-label']) !!}
                                {!! Form::text('tax', null, ['class' => 'form-control']) !!}
                            </div>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-group has-success">
                                {!! Form::label('quant', 'Cantidad disponible del producto', ['class' => 'control-label']) !!}
                                {!! Form::text('quant', null, ['class' => 'form-control']) !!}
                            </div>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                         
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-success">Crear</button>
                        </div>
                    </form>
                </div>



            </div>
        </div>
    </div>

@stop

@section('js')
    <script>
        $(document).ready(function() {
            $('#direcciones').DataTable({
                "serverSide": true,
                "ajax": "{{ url('api/products') }}",
                "columns": [{
                        data: "id"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "price"
                    },
                    {
                        data: 'tax'
                    }, 
                    {
                        data: 'quant'
                    },
                    {
                        data: 'options'
                    },

                ],
                order: [
                    [0, 'desc']
                ],
                responsive: true,
                autoWidth: false,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No hay registros para mostrar",
                    "info": "Mostrar página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "search": "Buscar",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)"
                }
            });
        });
    </script>
@stop
