@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('title', 'Dirección')

@section('content_header')
    <div class="card ">
        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> Dirección </h1>

            @can('Admin.direcciones.create')
                <div class="  ml-auto">
                    <div class="btn-group  ">
                        {{-- <a href="{{ route('direcciones.create') }}" class="btn btn-success">Crear Nueva</a> --}}
                        <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success">Crear
                            Nueva</button>
                    </div>
                </div>
            @endcan


        </div>
        <div class="card-footer">
            {{ Breadcrumbs::render('Admin.directions.trashed') }}
        </div>
    </div>
@stop

@section('content')

    @include('flash-message')

    <div class="container card p-5">
        <div class="row">
            <div class="   w-100" style="width:100%">

                <div class="card-body">

                    <table id="direcciones" class="table table-striped table-bordered" style="width:100%">
                        <thead class="thead-inverse">
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>

        </div>
    </div>

 

@stop

@section('js')
    <script>
        $(document).ready(function() {
            $('#direcciones').DataTable({
                "serverSide": true,
                "ajax": "{{ url('api/direccionesEliminadas') }}",
                "columns": [{
                        data: "id"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: 'options'
                    },

                ],
                order: [
                    [0, 'desc']
                ],
                responsive: true,
                autoWidth: false,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No hay registros para mostrar",
                    "info": "Mostrar página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)"
                }
            });
        });
    </script>
@stop
