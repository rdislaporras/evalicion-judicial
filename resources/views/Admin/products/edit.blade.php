@extends('adminlte::page')
    @section('content_header')
        <div class="card ">
            <div class="card-body   d-flex flex-row align-items-center justify-content-between">
                <h1> Editar Producto </h1>
                <div class="  ml-auto">
                    <div class="btn-group  ">
                        <a href="{{ route('Admin.products.index') }}" class="btn btn-success">Cacelar</a>
                    </div>
                </div>
            </div>
    
        </div>
    @stop

@section('content')
    <div class="container   p-5">
    <div class="card">
        <div class="card-body">
            <form class="bg-white "  action="{{ route('Admin.products.purchase', $product->id) }}"  method="post" enctype="multipart/form-data">
            @csrf     
            @method('PATCH')


                <input type="hidden" value="{{ $product->id }}" name="id">


                <div class="form-group">
                    <label for="name">Nombre del Producto</label>
                    <input class="form-control bg-light shadow-sm
                    @error('name') is-invalid @else border-0 @enderror"
                    id="name" name="name" placeholder="Ingresar dirección" value="{{ old('name', $product->name) }}">
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label for="price">Precio del Producto</label>
                    <input class="form-control bg-light shadow-sm
                    @error('price') is-invalid @else border-0 @enderror"
                    id="price" type="number" name="price" placeholder="Ingresar dirección" value="{{ old('price', $product->price) }}">
                    @error('price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label for="tax">Impuesto del Producto</label>
                    <input class="form-control bg-light shadow-sm
                    @error('tax') is-invalid @else border-0 @enderror"
                    id="tax" name="tax" type="number" placeholder="Ingresar dirección" value="{{ old('tax', $product->tax) }}">
                    @error('tax')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div> 
                
                <div class="form-group">
                    <label for="quant">Catntidad del Producto</label>
                    <input class="form-control bg-light shadow-sm
                    @error('quant') is-invalid @else border-0 @enderror"
                    id="quant" name="quant" type="number" placeholder="Ingresar dirección" value="{{ old('quant', $product->quant) }}">
                    @error('quant')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-success">Actualizar</button>
                </div>
            </form>
        </div>
    </div>
    </div>
@endsection