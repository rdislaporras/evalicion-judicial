@extends('adminlte::page')

@livewireStyles
@section('css') 
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}"> 
@stop

@section('plugins.Sweetalert2', true)

 

@section('title', 'Dashboard')
 

@section('content')


    <div class="  d-flex justify-content-center align-items-center p-3 text-center">


        <div class="container "  >
            <div class="row flex justify-content-center items-center" style="z-index:20">

                
               

            
                <div class="container card p-5">
                        <div class="row">

                            @include('flash-message')

                            <div class="   w-100" style="width:100%">

                                <div class="card-body">

                                    <table id="products" class="table table-striped table-bordered" style="width:100%">
                                        <thead class="thead-inverse">
                                            <tr>
                                                <th>Id</th>
                                                <th>Nombre</th>
                                                <th>Precio</th>
                                                <th>Inpuesto</th>
                                                <th>Cantidad</th>
                                                <th>Comprar</th>
                                            </tr>
                                        </thead>
                                    </table>

                                </div>

                            </div>

                        </div>
                    </div>

                 


 
            </div>
            
        </div>
    </div>

@stop


<script src="{{ asset('js/jquery.js') }}"></script>

@section('js')
    <script>
        $(document).ready(function() {
            $('#products').DataTable({
                "serverSide": true,
                "ajax": "{{ url('api/products.buy') }}",
                "columns": [{
                        data: "id"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "price"
                    },
                    {
                        data: 'tax'
                    }, 
                    {
                        data: 'quant'
                    },
                    {
                        data: 'buy'
                    },

                ],
                order: [
                    [0, 'desc']
                ],
                responsive: true,
                autoWidth: false,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No hay registros para mostrar",
                    "info": "Mostrar página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "search": "Buscar",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)"
                }
            });
        });
    </script>
@stop
 