@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap4.min.css') }}">
@stop


{{-- @section('plugins.Sweetalert2', true) --}}


@section('title', 'Correspondencias')
@section('content_header')


    <div class="card ">

        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> Correspondencias </h1>

            <div class="  ml-auto">
                <div class="btn-group  ">
                    {{-- <a class="btn btn-success" href="{{ URL::route('Admin.correspondence.create') }}">Crear Nuevo</a> --}}
                    {{-- <a class="btn btn-warning" href="{{ route('correspondencias.restore') }}"> Restaurar <i class="fas fa-recycle"></i></a> --}}
                </div>
            </div>

        </div>

        <div class="card-footer">
            {{ Breadcrumbs::render('correspondencias/todas') }}
        </div>


    </div>
@stop




@section('content')

    @include('flash-message')
 
    <div class="container card p-5">
        <div class="row">
            <div class="   w-100">

                <div class="card-body">

                    <table id="correspondencias" class="table table-bordered table-bordered table-striped  "
                        style="width:100%">
                        <thead class="thead-inverse">
                            <tr  class="text-center">
                                <th class="text-center">id</th>
                                <th class="text-center">N° Control</th>
                                <th class="text-center">F-Comuncd</th>
                                <th class="text-center">N° Comuncd</th>
                                <th class="text-center">Asunto</th>
                                <th class="text-center">F-Creación</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>

        </div>
    </div>
 
@stop

@section('js')

    <script src="{{ asset('/js/jquery-3.5.1.js') }}"></script>
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/js/responsive.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#correspondencias').DataTable({
                "serverSide": true,
                "ajax": "{{ url('api/correspondence.deleted') }}",
                "columns": [{
                        data: "id"
                    },
                    {
                        data: "n_control"
                    },
                    {
                        data: "f_comunc"
                    },
                    {
                        data: "n_comunc"
                    },
                    {
                        data: "asunto"
                    },
                    {
                        data: "created_at"
                    },
                    {
                        data: 'btn'
                    },
                ],
                responsive: true,
                autoWidth: false,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No hay registros para mostrar",
                    "info": "Mostrar página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "search": "Buscar",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)"
                }
            });
        });
    </script>




@stop
