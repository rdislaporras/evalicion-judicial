@extends('adminlte::page')



{{-- @section('plugins.Sweetalert2', true) --}}

@section('title', 'Users')
@section('content_header')

    <div class="card">

        <div class="card-body   d-flex flex-row align-items-center justify-content-between">
            <h1> Factura  # {{  $Invoice->id  }}</h1>
        </div> 
    </div>
@stop

@section('content')
    <div class="container col-6 p-5 ">
             <div class="row">
                <div class="card   p-5">

                    @include('Admin.invoices.partials.invoice')

                    <div class="col d-flex justify-content-center">

                        {{-- <a target="_blank" class="btn btn-danger mr-2"
                            href="{{ URL::route('Admin.correspondence.pdf', $data->id) }}">Crear PDF <i
                                class="fas fa-file-pdf text-white "></i></a>

                        <a class="btn btn-info" href="{{ route('Admin.correspondence.attachs', $data->id) }}" target="_blank">Descargar Documentos  <i class="fas fa-download ml-3"></i></a>
                        
                            <a class="btn btn-warning ml-2" href="{{ route('Admin.correspondence.index') }}"><i class="fas fa-reply-all mr-1"></i>Regresar</a>   --}}
                        
                    </div>
                </div>
            </div>
     </div>
@stop

@section('css') 
@section('js')
    <script>
     window.onbeforeunload = function () {
        window.scrollTo(0, 0);
    }
    </script>
@stop
