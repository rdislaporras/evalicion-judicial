@extends('adminlte::page')



{{-- @section('plugins.Sweetalert2', true) --}}

@section('title', 'Users')
@section('content_header')

    <div class="card">

            <div class="card-body   d-flex flex-row align-items-center justify-content-between">
                
                <h1> Editar Correspondencias </h1>

                <div class="  ml-auto">
                    <div class="btn-group  ">
                        <a class="btn btn-warning" href="{{ route('Admin.correspondence.index') }}"><i class="fas fa-reply-all mr-1"></i>Regresar</a>  
                    </div>
                </div>

            </div>

            <div class="card-footer">
                {{ Breadcrumbs::render('Admin.correspondencia.edit', $data->id) }}
            </div>

    </div>
@stop

@section('content')
    <div class="container card p-5">
             <div class="row">
                <div class="col p-5">
                    <form action="{{ url('/correspondencias') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        {{-- {{method_field('PATCH')}} --}}
                        @method('PUT')

                        <input type="hidden" value="{{ $data->prefijo }}" name="prefijo">
                        <input type="hidden" value="{{ $data->author_id }}" name="author_id">
                        <input type="hidden" value="{{ $data->id }}" name="id">
                        <input type="hidden" value="{{ $data->n_control }}" name="n_control">
                        <input type="hidden" value="{{ $state_id_actual }}" name="state_id_actual">
 
                        @include('Admin.correspondencias.partials.form')

                        <div class="col d-flex justify-content-center -bottom-3">

                            <button type="submit" class="btn btn-success mr-2">Actualizar</button>

                            <a class="btn btn-info "  href="{{ route('Admin.correspondence.attachs', $data->id) }}" target="_blank" formtarget="_blank" >
                                Descargar Adjuntos 
                                <i class="ml-2 fas fa-download text-black"></i>
                            <a>
                            
                            <a class="btn btn-warning ml-2 " href="{{ route('Admin.correspondence.index') }}"><i class="fas fa-reply-all mr-1"></i>Regresar</a>  

                        </div>

                    </form>
                </div>
            </div>
     </div>
@stop


@section('js')

    <script>
        if ($.trim($('#f_archivd').val()).length !== 0) {


            console.log("tiene data");
            $('#archivar').prop('checked', true);
            $('#f_archivd').prop('disabled', false);
            // $('#f_archivd').prop('readonly', false);

            $('#archivar').on('click', function() {

                $('#f_archivd').prop('disabled', false);

            });

        } else {


            console.log("nulo");
            $('#archivar').prop('checked', false);
            $('#f_archivd').prop('disabled', true);
            // $('#f_archivd').prop('readonly', true);

            $('#archivar').on('click', function() {
                if ($('#f_archivd').prop('disabled') == false) {
                    $('#f_archivd').prop('disabled', true);
                } else {
                    $('#f_archivd').prop('disabled', false);
                }
            });

        }
    </script>


@stop
