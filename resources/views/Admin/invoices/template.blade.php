<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        body {
            font-size: 10px;
            font-family: "Arial";
        }
    </style>
</head>

<body syle="">



    <table  cellpadding="3"   syle="width:100% ">
        <tr style="width:100%; ">
            
            <td style="float: left; width: 25%; text-align:left; margin:0px;  ">
                <p style="margin:0px;  "> 
                    <img src="img/logo.png" style="width: 90px; margin:0px; "> 
                </p>
            </td>
            
            <td style="width: 50%;  vertical-align: middle;text-align:center">
                <h1 style="margin:0; vertical-align: middle;">Hoja de Acción</h1>
                <h3 style="margin:0; vertical-align: middle;">PRESIDENCIA</h3>
                <p style="margin:0; vertical-align: middle; font-weight:bold">N° CONTROL: {{ $data->n_control }} </p>
            </td>
           
            <td style="float: right; width: 25%; text-align:right; margin:0px; ">
                <p style="margin:0px; ">
                     <img src="img/minc.png" style="width: 90px; margin:0px;">
                </p>
            </td>

        </tr>
    </table>

    <br>
    <br>
    <br>
    <br>

    <table cellpadding="3">
        <tr>
            <td style="width: 23%;"><b>Remitente:</b></td>
            <td style="width: 77%; text-align: justify;"> {{ $data->remitter }}</td>
        </tr>
        <tr>
            <td><b>Fecha Recibido:</b></td>
            <td> {{ $data->f_recibo }} </td>
        </tr>
        <tr>
            <td><b>Hora Recibido:</b></td>
            <td >{{ $data->h_recibo }}  </td>
        </tr>
        <tr>
            <td><b>N° Comnc:</b></td>
            <td>{{ $data->n_comunc }}</td>
        </tr>
        <tr>
            <td><b>Fecha Comnc:</b></td>
            <td >{{ $data->f_comunc }}</td>
        </tr>
        <tr>
            <td><b>Tipología:</b></td>
            <td style="text-align: justify;">{{ $data->typology }}</td>
        </tr> 
        <tr>
            <td><b>Gerencia:</b></td>
            <td style="text-align: justify;">{{$data->direction}}</td>
        </tr> 
        <tr>
            <td><b>Asunto:</b></td>
            <td style="text-align: justify;">{{ $data->asunto }}</td>
        </tr>  
 
    </table>
 
 
    <br>
    <br>

    @if ($data->accion == null)  
             
        <div></div>
        <h4>INSTRUCCIONES / OBSERVACIONES</h4>
        <div style="line-height:15px;">________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</div>
            
    @else 
        <div></div>
        <h4>INSTRUCCIONES / OBSERVACIONES</h4>
        <div style="line-height:12px; text-align: justify; text-decoration: underline;">
            {{$data->accion}}
        </div> 
    @endif

    <br>
    <br>

    <h4 >ACCIÓN INTERNA</h4>
    <table   cellpadding="4">
        <tr>
            <td style="width: 70px;vertical-align: middle;">
                <img  src="img/box16.jpg" style="width: 10px; " > 
                Archivar
            </td>
            <td style="width: 180px;">
                <img  src="img/box16.jpg" style="width: 10px; " > 
                Información y Fines Consiguientes
            </td>
            <td>
                <img  src="img/box16.jpg" style="width: 10px; " > 
                Solicitud de Audiencia
            </td>
        </tr>
    </table>  
            
                  
</body>

</html>
