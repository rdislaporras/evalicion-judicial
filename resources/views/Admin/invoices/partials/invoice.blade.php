<div class="container">
    <div class="row">

        <div class=" font-bold d-flex flex-row align-items-center justify-content-between">
            <h3> Factura  #{{ $Invoice->id }}</h3>
        </div> 


        <table class="table">
            <thead>
                <tr>
                    <th>Producto</th>
                    <th>Subtotal</th>
                    <th>Impuesto ({{  $Product->tax  }}%)</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> {{  $Product->name  }}</td> 
                    <td> {{  $percent = ($Product->price) - ($Product->price*$Product->tax/100);   }}$</td> 
                    <td> {{    $Product->price*$Product->tax/100 }}$</td>   
                    <td> {{  $Product->price  }}$</td> 
                </tr>
                 
            </tbody>
        </table>

    </div>
</div>
 









    