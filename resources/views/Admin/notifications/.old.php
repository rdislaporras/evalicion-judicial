@extends('adminlte::page')

@section('css')
{{-- <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('/css/app.css') }}">
<link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap4.min.css') }}">
@stop



@section('title', 'Correspondencias')

@section('content_header')


<div class="card ">

    <div class="card-body   d-flex flex-row align-items-center justify-content-between">
        <h1> Notificaciones </h1>
    </div>

    <div class="card-footer">
        {{ Breadcrumbs::render('notifications') }}
    </div>


</div>
@stop




@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="card  w-100">

            <div class="container-fluid">
                <div class="row">


                    <div class="card col ">

                        <div class="card-header ">
                            <h4 style="  ">
                                Bandeja de Notificaciones
                            </h4>

                        </div>

                        <div class="container-fluid">
                            <div class="row">
                                <div class="card  w-100">

                                    <div class="card-body">

                                        <table id="notificaciones"
                                            class="table table-bordered table-bordered table-striped  "
                                            style="width:100%">
                                            <thead class="thead-inverse">
                                                <tr class="text-center">
                                                    <th class="text-center">id</th>
                                                    <th class="text-center">F. Creación</th>
                                                    <th class="text-center">N° Control</th>
                                                    <th class="text-center">Opciones</th>
                                                </tr>
                                            </thead>
                                        </table>

                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

@stop

@section('js')

<script src="{{ asset('/js/jquery-3.5.1.js') }}"></script>
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/js/responsive.bootstrap4.min.js') }}"></script>

<script>
$(document).ready(function() {
    $('#notificaciones').DataTable({
        "serverSide": true,
        "ajax": "{{ url('api/notificaciones') }}",
        "columns": [{
                data: "id"
            },

            {
                data: "created_at"
            },
            {
                data: "data['n_control']"
            },
            {
                data: 'options'
            },

        ],
        order: [
            [0, 'desc']
        ],
        responsive: true,
        autoWidth: true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No hay registros para mostrar",
            "info": "Mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "search": "Buscar",
            "infoFiltered": "(filtrados de _MAX_ registros totales)"
        }
    });
});
</script>




@stop