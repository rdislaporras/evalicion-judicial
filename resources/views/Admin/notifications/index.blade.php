@extends('adminlte::page')

@section('css')
{{-- <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('/css/app.css') }}">
<link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap4.min.css') }}">
@stop



@section('title', 'Correspondencias')

@section('content_header')


<div class="card ">

    <div class="card-body   d-flex flex-row align-items-center justify-content-between">
        <h1> Notificaciones </h1>
    </div>

    <div class="card-footer">
        {{ Breadcrumbs::render('notifications') }}
    </div>


</div>
@stop




@section('content')

@include('flash-message')

<div class="container-fluid">
    <div class="row">
        <div class="card  w-100">

            <div class="container-fluid">
                <div class="row">

                    <div class="  col ">

                        <div class="card-header ">
                            <h4 style="  ">
                                Bandeja de Notificaciones
                            </h4>

                        </div>

                        <div class="card-body">

                            @foreach (auth()->user()->Notifications as $notification)

                            <a href="{{ route('Admin.correspondence.show',$notification->data['mail_id']) }}" data-id="{{ $notification->id }}" class="alert   {{ $notification->read_at ? "border-success":"border-danger" }}  d-flex align-items-center justify-content-between alert-dismissible fade show " role="alert">

                                <small class="text-muted">
                                    [{{ $notification->created_at }}]
                                </small>


                                <p class="m-0 {{ $notification->data['state'] == 'Creada' ? 'text-info':'text-warning'  }}  ">
                                    Correspondencia {{ $notification->data['state'] }}
                                    <strong class="text-dark">#{{$notification->data['n_control']}}</strong>
                                </p>
                                </p>

                                <div class="d-flex justify-content-around items-center">

                                    @if($notification->read_at)
                                    <div class=" text-success reed-notification  mark-as-read mr-1" data-id="{{ $notification->id }}">
                                        Revisar
                                    </div>
                                    @else
                                    <div class=" text-success reed-notification  mark-as-read mr-1" data-id="{{ $notification->id }}">
                                        Leer
                                    </div>
                                    @endif

                                    @if($notification->read_at)
                                    <div class=" text-danger delete-notification btn-close ml-1" data-dismiss="alert" data-id="{{ $notification->id }}">
                                        Borrar
                                    </div>
                                    @endif


                                </div>

                                @if($notification->read_at)
                                <i class="fas fa-check-double text-success" style="position: absolute;top:50%;right:10px; transform:translate(-50%,-50%)"></i>
                                @else
                                <i class="fas fa-check text-muted" style="position: absolute;top:50%;right:10px; transform:translate(-50%,-50%)"></i>
                                @endif


                            </a>



                            @endforeach

                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

@stop



<script src="{{ asset('js/jquery.js') }}"></script>