<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>


    <style>
        .container-fit-center{
            width:fit-content;
            display:flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .container-fluid-center{
            width:100%;
            display:flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .container-header{
            width:100%;
            display:flex;
            justify-content: center;
            align-items: center;
        }
        .container-info{
            width:fit-content;
            display:flex;
            flex-direction: column;
            align-items: flex-start;
            margin-left: 2rem;
        }

    </style>
</head>

<body>


    <div class="card container-fit-center" style="border: 1px solid #198754; background: #198754e8;color:white;padding-top:0.5rem;">

        <div class="container-header">
             <img src="{{$message->embed('img/logo.png')}}" style="width: 90px; margin:0px; "> 

             <div class="container-info">
                 <h2 style="font-size:6vw;z-index:1;border-bottom:2px solid white; color:white;margin:0px;"> 
                    <strong class="text-white;text-shadow: 5px 3px 6px black;">SIC</strong> 
                    {{-- <small class="text-white" style="font-size: 2rem; ">v2.0</small> --}}
                </h2>
                <h4 class="text-white" style="text-transform: none;margin-top:1rem;">Sistema Integral de Correspondencia</h4>   
             </div>
        </div>

        <div class="container-fluid-center" style="text-align: left">

           

            <br>

            <h2>Correspondencia Actualizada </h2>
            <h4>Editado por: {{$data['editor']}} </h4>

            {{-- <h2>{{$data['title']}}</h2> --}}
            <h3> Numero de Control: <b>{{$data['n_control']}}</b></h3>

            <a class="ml-2 mr-2"  href="{{ $data['link'] }}" style ="color: white; background: #198754; padding: 20px; text-decoration: none; border: 1px solid;">
                Ver correspondencia  
                <strong>
                    {{-- {{$data['n_control']}} --}}
                </strong>
            </a>



            {{-- <P>{{$data['body_title']}} <strong>{{$data['state_name']}} </strong></P> --}}

            {{-- <br>    --}}
{{-- 
            <h4>Description:</h4>
            <p>{{$data['asunto']}}</p>

            <h4>Historial:</h4>
            <p>{{$data['history']}}</p> --}}

        </div>
             
        <br>
        <br>       

        <div class="container-fluid-center">

            <small class="m-0">Antes de imprimir este mensaje, asegúrese de que es necesario.</small>                
            <small class="m-0">"Por cada árbol que se tala, por cada animal que muere,</small>
            <small class="m-0">el planeta sufre heridas profundas difíciles de cicatrizar".</small>   
            <small class="m-0">¡Salvemos el Planeta!</small> 
            
            <br> 

            <img src="{{$message->embed('img/footer.jpg')}}" style="width: 100%; "> 

        </div>
 
    </div>


</body>

</html>