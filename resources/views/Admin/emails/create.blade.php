<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>


    <style>
        .container-fit-center{
            width:fit-content;
            display:flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .container-fluid-center{
            width:100%;
            display:flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .container-header{
            width:100%;
            display:flex;
            justify-content: center;
            align-items: center;
        }
        .container-info{
            width:fit-content;
            display:flex;
            flex-direction: column;
            align-items: flex-start;
            margin-left: 2rem;
        }

    </style>
</head>

<body>


    <div class="card container-fit-center" style="border: 1px solid #198754; background: #198754e8;color:white;padding-top:0.5rem;">

        <div class="container-header">
             <img src="{{$message->embed('img/logo.png')}}" style="width: 90px; margin:0px; "> 

             <div class="container-info">
                 <h2 style="font-size:6vw;z-index:1;border-bottom:2px solid white; color:white;margin:0px;"> 
                    <strong class="text-white;text-shadow: 5px 3px 6px black;">EVALUACION</strong> 
                    {{-- <small class="text-white" style="font-size: 2rem; ">v2.0</small> --}}
                </h2>
              </div>
        </div>

        <div class="container-fluid-center" style="text-align: left">

            <h2>Compra Realizada</h2>

            {{-- <h2>{{$data['title']}}</h2> --}}
            <h3> Numero de compra: <strong>{{$data['n_control']}}</strong></h3>

            <a class="ml-2 mr-2"  href="{{ $data['link'] }}" style ="color: white; background: #198754; padding: 20px; text-decoration: none; border: 1px solid;">
                Ver compra   
            </a>
 
        </div>
       
         
 
    </div>


</body>

</html>