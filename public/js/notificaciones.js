jQuery(document).ready(function ($) {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    function sendMarkRequest(id = null) {
        $.ajax("{{ route('Admin.notifications.markAsRead') }}", {
            method: "POST",
            data: {
                id,
            },
        }).done(function (res) {
            console.log(res);
        });
    }

    function deleteRequest(id = null) {
        $.ajax("{{ route('Admin.notifications.delete') }}", {
            method: "POST",
            data: {
                id,
            },
        }).done(function (res) {
            console.log(res);
        });
    }

    $(".mark-as-read").click(function () {
        let request = sendMarkRequest($(this).data("id"));
    });

    $(".delete-notification").click(function () {
        let request = deleteRequest($(this).data("id"));
    });
});
