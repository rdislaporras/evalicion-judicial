<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correspondence', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->bigIncrements('id');

            $table->unsignedInteger('tipology_id')->nullable();
            $table->unsignedInteger('direction_id')->nullable();
            $table->unsignedInteger('remitter_id')->nullable();

            $table->integer('id_userDirection')->nullable();
            $table->text('n_control')->nullable();
            $table->bigInteger('n_comunc');
            $table->date('f_recibo')->useCurrent()->nullable();
            $table->time('h_recibo')->default('08:00:00')->nullable();
            $table->date('f_comunc');
            $table->integer('prefijo');
            $table->boolean('archivar')->nullable();
            $table->date('f_archivd')->nullable();
            $table->longText('asunto');
            $table->longText('accion');
            $table->longText('ubicacion');
            $table->longText('observaciones');


            $table->foreign('tipology_id')->references('id')->on('typologies')->onDelete('set null');
            $table->foreign('direction_id')->references('id')->on('directions')->onDelete('set null');
            $table->foreign('remitter_id')->references('id')->on('remitters')->onDelete('set null');

            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->useCurrent()->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('correspondence');
    }
};