<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    //  use DisableForeignKeys, TruncateTable;
    //  use TruncateTable;


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // $this->disableForeignKeys();

        // DB::table('products')->truncate();



        $products = [
            [
                'name'     => 'Teclado RGB',
                'price'  => '10',
                'tax'  => '3',
                'quant'  => '15'
            ],
            [
                'name'     => 'Mouse Gamer',
                'price'  => '12',
                'tax'  => '2',
                'quant'  => '11'
            ],
            [
                'name'     => 'Memoria RAM 16GB',
                'price'  => '20',
                'tax'  => '4',
                'quant'  => '6'
            ],
            [
                'name'     => 'Disco Duro SSH 128gb',
                'price'  => '40',
                'tax'  => '3',
                'quant'  => '4'
            ],
            [
                'name'     => 'Monitor 4k 54"',
                'price'  => '240',
                'tax'  => '8',
                'quant'  => '4'
            ],


        ];

        DB::table('products')->insert($products);

        // $this->enableForeignKeys();

    }
}