<?php
namespace Database\Seeders;


use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermisosSeeder extends Seeder
{
    // use DisableForeignKeys;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->disableForeignKeys();
        // DB::table('permissions')->truncate();

        // $permissions = [
        //     [
        //         'name'     => 'view backend',
        //         'guard_name'  => 'web',
        //         'description' => 'Permiso base de todo usuario',
        //         'created_at' => date('Y-m-d h:m:s')
        //     ],
        //     [
        //         'name'     => 'admin',
        //         'guard_name'  => 'web',
        //         'description' => 'Administrador del sitio',
        //         'created_at' => date('Y-m-d h:m:s')
        //     ],
        //     [
        //         'name'     => 'correspondencias',
        //         'guard_name'  => 'web',
        //         'description' => 'Permiso para crear Correspondencias',
        //         'created_at' => date('Y-m-d h:m:s')
        //     ],
        //     [
        //         'name'     => 'directions',
        //         'guard_name'  => 'web',
        //         'description' => 'Permiso para crear Direcciones',
        //         'created_at' => date('Y-m-d h:m:s')
        //     ],
            // [
            //     'name'     => 'ejecucion',
            //     'guard_name'  => 'web',
            //     'description' => 'Permiso para gestionar las asistencias y notas',
            //     'created_at' => date('Y-m-d h:m:s')
            // ],
            // [
            //     'name'     => 'reportes',
            //     'guard_name'  => 'web',
            //     'description' => 'Permiso para ver reportes',
            //     'created_at' => date('Y-m-d h:m:s')
            // ],
            // [
            //     'name'     => 'stock',
            //     'guard_name'  => 'web',
            //     'description' => 'Permiso para el manejo de stock de materiales',
            //     'created_at' => date('Y-m-d h:m:s')
            // ],
        // ];

        // DB::table('permissions')->insert($permissions);

        // $per = DB::table('model_has_permissions')->where('permission_id',2)->where('model_id',1)->first();
        // //dd($per);
        // if ($per==null){
        //     DB::table('model_has_permissions')->insert([
        //         'permission_id' => 2,
        //         'model_type' => 'App\Models\Auth\User',
        //         'model_id' => 1]
        //     );
        // }
        


        // $this->enableForeignKeys();
    }
}

