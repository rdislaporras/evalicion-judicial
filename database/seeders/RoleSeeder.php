<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $Admin          = Role::create(['name' => 'Admin']);
        $Client        = Role::create(['name' => 'Client']);





        Permission::create(['name' => 'escritorio'])->syncRoles([$Admin, $Client]);


        Permission::create(['name' => 'Admin.user.index'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.user.all'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.user.create'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.user.store'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.user.edit'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.user.show'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.user.update'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.user.delete'])->syncRoles([$Admin]);
        // Permission::create(['name' => 'Admin.user.forceDelete'])->syncRoles([$Admin]);
        // Permission::create(['name' => 'Admin.user.restore'])->syncRoles([$Admin]);


        Permission::create(['name' => 'Admin.products.index'])->syncRoles([$Admin, $Client]);
        // Permission::create(['name' => 'Admin.products.MailxDir'])->syncRoles([$Client]);
        // Permission::create(['name' => 'Admin.products.all'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.products.create'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.products.store'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.products.show'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.products.edit'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.products.update'])->syncRoles([$Admin]);
        Permission::create(['name' => 'Admin.products.delete'])->syncRoles([$Client]);
        // Permission::create(['name' => 'Admin.products.deleted'])->syncRoles([$Admin]);
        // Permission::create(['name' => 'Admin.products.forceDelete'])->syncRoles([$Admin]);
        // Permission::create(['name' => 'Admin.products.restore'])->syncRoles([$Admin]);
        // Permission::create(['name' => 'Admin.products.pdf'])->syncRoles([$Admin, $Client]);
        Permission::create(['name' => 'Admin.products.buy'])->syncRoles([$Admin, $Client]);
        Permission::create(['name' => 'Admin.products.purchase'])->syncRoles([$Admin, $Client]);
    }
}