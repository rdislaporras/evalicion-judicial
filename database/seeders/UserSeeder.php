<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin User',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123'),
        ])->assignRole(['Admin']);

        User::create([
            'name' => 'Cliente Prueba',
            'email' => 'cliente@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123'),
        ])->assignRole('client');
    }
}