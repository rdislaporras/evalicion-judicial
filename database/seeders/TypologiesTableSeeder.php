<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypologiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         DB::table('typologies')->truncate();

        $typologies = [
            
            ['name' => 'AUDIENCIA'],
            ['name' => 'BORRADORES PARA LA FIRMA'],
            ['name' => 'CURRICULUM'],
            ['name' => 'DENUNCIAS'],
            ['name' => 'DESIGNACIÓN DE FUNCIONARIOS'],
            ['name' => 'EXONERACIONES'],
            ['name' => 'INFORME Y PRODUCTOS DE ASESORES'],
            ['name' => 'INVITACIÓN / CONVOCATORIA'],
            ['name' => 'MEMORANDO'],
            ['name' => 'NORMAL'],
            ['name' => 'OFICIO'],
            ['name' => 'RECURSO JERÁRQUICO'],
            ['name' => 'REPOSOS / CONSTANCIAS MÉDICAS'],
            ['name' => 'VACACIONES /PERMISOS'] 
        ];

         DB::table('typologies')->insert($typologies);


 
    }
}
