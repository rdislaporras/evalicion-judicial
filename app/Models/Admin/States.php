<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class States extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['id','name']; // Identificamos los campo que tenemos en la bd


    public function correspondencias(){
        return $this->hasMany('App\Models\Admin\Correspondence');
    }

}
