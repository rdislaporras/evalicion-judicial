<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Purchases extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['id', 'user_id', 'product_id'];

    // public function User()
    // {
    //     return $this->  ('App\Models\User', 'user_id');
    // }

    // public function Product ()
    // {
    //     return $this->belongsTo('App\Models\Admin\Products', 'product_id');
    // }
}