<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoices extends Model
{

    use HasFactory;
    use SoftDeletes;

    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $fillable = ['id', 'purchase_id', 'pending'];
}