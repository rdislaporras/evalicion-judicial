<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Correspondence extends Model
{


    use HasFactory, SoftDeletes;


    protected $table = "correspondence";

    protected $primaryKey = 'id';

    protected $fillable =
    [
        'prefijo',
        'n_control',
        'id_userDirection',
        'f_recibo',
        'h_recibo',
        'n_comunc',
        'f_comunc',
        'tipology_id',
        'remitter_id',
        'direction_id',
        'archivar',
        'f_archivd',
        'asunto',
        'accion',
        'ubicacion',
        'observaciones',
        'state_id',
        'history',
        'attachtmentName',
        'attachtmentPath',
        'author_id',
    ];


    public function tipologies()
    {
        return $this->belongsTo('App\Models\Admin\Typology', 'tipology_id');
    }

    public function directions()
    {
        return $this->belongsTo('App\Models\Admin\directions', 'directions_id');
    }

    public function remitters()
    {
        return $this->belongsTo('App\Models\Admin\Remitters', 'remitters_id');
    }

    public function states()
    {
        return $this->belongsTo('App\Models\Admin\States', 'state_id');
    }
}