<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Notifications\CorrespondenceNotification;
use App\Models\Admin\Notifications;
use App\Models\User;
use Carbon\Carbon;


use App\Mail\CorrespondenceMailable;
use App\Mail\CreateCorrespondenceMailable;
use Illuminate\Support\Facades\Mail;

use App\Models\Admin\Direction;


use Auth;

class NotificationsController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('Admin.notifications.index');
    }

    public function notify()
    {


        // if( auth()->user() ){
        //     $user = User::first(); 
        //     auth()->user()->notify(new CorrespondenceNotification($user));
        // }


        $data = [
            'text' => ' Corrspnc. ',
            'n_control' => '22-1202',
            'state' => 'nueva',
            'mail_id' => '1',
        ];


        auth()->user()->notify(new CorrespondenceNotification($data));


        dd('done');
    }

    public function check($id, $mail)
    {

        // return response()->json($mail);

        $userUnreadNotification = auth()->user()->unreadNotifications->when($id, function ($query) use ($id) {
            return $query->where('id', $id);
        });

        $userUnreadNotification->markAsRead();
        return redirect()->route('Admin.correspondence.show',  $mail);
    }

    public function markAsRead(Request $request)
    {


        auth()->user()->unreadNotifications->when($request->input('id'), function ($query) use ($request) {
            return $query->where('id', $request->input('id'));
        })->markAsRead();

        return response()->noContent();
    }

    public function delete(Request $request)
    {



        $data = Notifications::query()->find($request->input('id'));
        $data->delete();

        return response()->noContent();
    }

    public function getNotificationsData(Request $request)
    {


        $notifications = auth()->user()->unreadNotifications;


        // Now, we create the notification dropdown main content.
        $now = Carbon::now();

        $dropdownHtml = '';

        foreach ($notifications as $key => $item) {



            $icon = "<i class='mr-2 fas fa-fw fa-envelope text-primary'></i>";

            $createAt = $item->created_at;




            $diffInHours = $createAt->diffInHours($now);
            $diffInSeconds = $createAt->diffInSeconds($now);
            $diffInMinutes = $createAt->diffInMinutes($now);
            $diffInDays = $createAt->diffInDays($now);



            if ($diffInMinutes > 4320) {
                $diff = $diffInDays . " Ds.";
                $time = "<span class='float-right text-danger text-sm  mr-1 badge'>{$diff}</span>";
            } else {
                if ($diffInMinutes > 1440) {
                    $diff = $diffInDays . " Ds.";
                    $time = "<span class='float-right text-warning text-sm  mr-1 badge'>{$diff}</span>";
                } else {
                    if ($diffInMinutes > 59) {
                        $diff = $diffInHours . " Hr.";
                        $time = "<span class='float-right text-success text-sm  mr-1 badge'>{$diff}</span>";
                    } else {
                        if ($diffInMinutes > 1) {
                            $diff = $diffInMinutes . " Min.";
                            $time = "<span class='float-right text-muted text-sm  mr-1 badge'>{$diff}</span>";
                        } else {
                            $diff = $diffInSeconds . " Seg.";
                            $time = "<span class='float-right text-muted text-sm  mr-1 badge'>{$diff}</span>";
                        }
                    }
                }
            }


            $n_control = "<strong class='ml-1'> {$item->data['n_control']}</strong>";
            $state = "<span class='text-success'>{$item->data['state']}</span>";
            $text = $item->data['text'];

            $dropdownHtml .= "<a class='alert alert-link notiLink  ' href='/notifications/id/{$item->id}/mail/{$item->data['mail_id']}' class='bell-notify d-flex jusctify-content-around dropdown-item align-items-center p-1 delete-notification' style='width:fit-content;'> {$icon} {$time} <small>{$text} {$state} {$n_control}</small></a>";

            if ($key < count($notifications) - 1) {
                $dropdownHtml .= "<hr>";
            }
        };


        // Return the new    notification data.  

        return response()->json(

            [
                'label' => count(auth()->user()->unreadNotifications),
                'label_color' => 'danger',
                'icon_color' => 'dark',
                'dropdown' => $dropdownHtml,
            ]

        );
    }
}