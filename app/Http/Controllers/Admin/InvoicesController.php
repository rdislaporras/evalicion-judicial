<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Invoices;
use App\Models\Admin\Products;
use App\Models\Admin\Purchases;

// use App\Models\User;
// use Carbon\Carbon;




use Auth;

class InvoicesController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        // $Purchases = Purchases::get();
        // $invoices = Invoices::get();
        // return response()->json($invoices);


        return view('Admin.invoices.index');
    }

    public function get()
    {
        // $data = Correspondence::where('direction_id', Auth::user()->id)->get();

        $invoices = Invoices::get();

        return datatables()->of($invoices)
            ->addColumn('pending', 'Admin/invoices/partials/pending')
            ->addColumn('options', 'Admin/invoices/partials/options')
            ->rawColumns(['pending', 'options'])
            ->toJson();
    }




    public function show($id)
    {
        $Invoice = Invoices::FindOrFail($id);
        $purchase = Purchases::FindOrFail($Invoice->purchase_id);
        $Product = Products::FindOrFail($purchase->product_id);

        // //  return view('Admin.correspondencias.edit', compact('data'));
        return view('Admin.invoices.show')
            ->with('Invoice', $Invoice)
            ->with('purchase', $purchase)
            ->with('Product', $Product);
    }
}