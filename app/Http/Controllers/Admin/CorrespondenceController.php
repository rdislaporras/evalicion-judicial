<?php


namespace App\Http\Controllers\Admin;

use App\Models\Admin\Correspondence;
use App\Models\Admin\Typology;
use App\Models\Admin\Direction;
use App\Models\Admin\Remitter;
use App\Models\Admin\States;
use App\Models\User;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
// use Illuminate\Support\Facades\Response as FacadeResponse;





use App\Mail\CorrespondenceMailable;
use App\Mail\CreateCorrespondenceMailable;
use App\Mail\UpdateCorrespondenceMailable;

use App\Notifications\CorrespondenceNotification;

use Auth;
use PDF;

class CorrespondenceController extends Controller
{

    use Notifiable;


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        // return response()->json(Auth::user());

        return view('Admin.correspondencias.index');
    }


    public function MailxDir()
    {

        // if ((Auth::user()->direction_id === 3) || (Auth::user()->direction_id === null)) {
        if ((Auth::user()->direction_id == 2)) {
            $data = Correspondence::get();
            // return response()->json($data);

            return datatables()->of($data)
                ->addColumn('options', 'Admin/correspondencias/partials/options')
                ->rawColumns(['options'])
                ->toJson();
        } else {
            //     // $data = Correspondence::where('id_userDirection', '=', Auth::user()->direction_id)->get();
            $data = Correspondence::where('direction_id', Auth::user()->direction_id)->get();

            return datatables()->of($data)
                ->addColumn('options', 'Admin/correspondencias/partials/options')
                ->rawColumns(['options'])
                ->toJson();
        }
    }




    public function create()
    {
        // dd(auth()->user());
        // return response()->json(Auth::user());

        $date = date('Y');
        $prefix  = substr($date, 2, 3);


        $count = Correspondence::all()->count();

        $count === 0 ?
            $newReg = 1
            :
            $newReg = Correspondence::orderBy('id', 'desc')->first()->id + 1;




        $userId = Auth::id();
        $userName = Auth::user()->name;



        $data['prefix']  = substr($date, 2, 3);
        $data['newReg']  =  $newReg;


        $n_control =  $prefix . "-" . $newReg;

        $tipologias = Typology::all();
        $remitentes = Remitter::all();
        $direcciones = Direction::all();
        $states = States::pluck('name', 'id');


        $userdirection_id = Auth::user()->direction_id;

        if (!$userdirection_id) {
            return redirect()->route('Admin.correspondence.index')->with('danger', 'Usuario autor no se encuentra asignado a una gerencia. Por favor contacte al personal de soporte.');
        }
        // return response()->json($userdirection_id);


        $UserDirectionName = Direction::FindOrFail($userdirection_id);


        return view('Admin.correspondencias.create')
            ->with('newReg', $newReg)
            ->with('prefix', $prefix)
            ->with('userId', $userId)
            ->with('userName', $userName)
            ->with('n_control', $n_control)
            ->with('tipologias', $tipologias)
            ->with('remitentes', $remitentes)
            ->with('direcciones', $direcciones)
            ->with('userdirection_id', $userdirection_id)
            ->with('UserDirectionName', $UserDirectionName)
            ->with('states', $states);
    }


    public function storeFile($data, $input)
    {

        if ($input) {

            $filePath = 'adjuntos/' . $request->n_control . "/";


            $file = $request->file('attachtment');

            $fileName =  date('d-m-Y', strtotime("now")) . "_" . $file->hashName();

            $fileLink = str_replace(' ', '_', $fileName);



            if (Storage::disk('public')->exists($filePath . $fileName)) {
                return redirect()->route('Admin.correspondence.index')->with('warning', 'El archivo que intenta adjuntar ya se encuentra guardado en el sistema, por favor valide su nombre e intente nuevamente');
            } else {
                $file->storeAs($filePath, $fileName, 'public');
            }

            $request->request->add(['attachtmentName' => $fileName]);
            $request->request->add(['attachtmentPath' => $filePath]);

            $data = $request->except(['_token', 'newReg']);


            Correspondence::create($data);
        } else {

            $data = $request->except(['_token', 'newReg']);
            Correspondence::create($data);
        }
    }


    public function store(Request  $request)
    {

        $input = $request->hasFile('attachtment');

        // $this->storeFile($data, $input);


        // Guardo el archivo subido al store 
        if ($request->hasFile('attachtment')) {

            $filePath = 'adjuntos/' . $request->n_control . "/";


            $file = $request->file('attachtment');

            $fileName =  date('d-m-Y', strtotime("now")) . "_" . $file->hashName();

            $fileLink = str_replace(' ', '_', $fileName);



            if (Storage::disk('public')->exists($filePath . $fileName)) {
                return redirect()->route('Admin.correspondence.index')->with('warning', 'El archivo que intenta adjuntar ya se encuentra guardado en el sistema, por favor valide su nombre e intente nuevamente');
            } else {
                $file->storeAs($filePath, $fileName, 'public');
            }

            $request->request->add(['attachtmentName' => $fileName]);
            $request->request->add(['attachtmentPath' => $filePath]);
        }

        // registro la correspondencia
        Correspondence::create($request->except(['_token', 'newReg']));


        // envio email avisando sobre la creación de la correspondencia a ambas gerencias
        $states = States::FindOrFail($request->state_id);

        $request->request->add(['title' => "Notificación de Correspondencia"]);
        $request->request->add(['description' => "Se ha creado una correspondencia con el N° de control"]);
        $request->request->add(['body_title' => "El estado de la correspondencia es: "]);
        $request->request->add(['state_name' => $states->name]);
        $request->request->add(['link' => route('Admin.correspondence.show', $request->newReg)]);



        $Gerencia = Direction::FindOrFail($request->direction_id);
        // return response()->json($Gerencia);

        $creatorGerencia = Direction::FindOrFail($request->id_userDirection);

        Mail::to($creatorGerencia->email)->send(new CreateCorrespondenceMailable($request));
        Mail::to($Gerencia->email)->send(new CreateCorrespondenceMailable($request));

        //si la gerencia del autor es distinta a la de shantall le debe enviar un email a ella (id gerencia shantall 1 )
        if ($creatorGerencia->id != User::where('id', '2')->first()->direction_id) {
            Mail::to(User::where('id', '2')->first()->email)->send(new CreateCorrespondenceMailable($request));
        }




        // Genero las notificaciones para los usuarios asociados a las gerencias

        $dataNotify = [
            'text' => ' Corrspnc. ',
            'n_control' => $request->n_control,
            'state' => 'Creada',
            'mail_id' => $request->newReg,
        ];


        auth()->user()->notify(new CorrespondenceNotification($dataNotify)); //envio notificacion a la gerencia autora

        //envio notificacion a shantall si es otro usuario distinto al de shantall
        if (auth()->user()->id != 2) {

            $user2 =  User::where('id', '2')->first(); //me traigo a shantall
            // return response()->json($user2); //le envio la notificacion a shantall

            $user2->notify(new CorrespondenceNotification($dataNotify));
        }


        if ($user1 =  User::where('direction_id', $Gerencia->id)->first()) {
            $user1->notify(new CorrespondenceNotification($dataNotify));  //envio notificacion a la gerencia dirigida
        } else {
            return redirect()->route('Admin.correspondence.index')->with('warning', 'Corrrespondencia #' . $request['n_control'] . ' creada satisfactoriamente, pero la gerencia destinataria no cuenta con un usuario asociado. Por favor comunique este mensaje a su supervisor inmediato');
        }

        // return response()->json($user);


        // // return response()->json($data); 
        return redirect()->route('Admin.correspondence.index')->with('success', 'Corrrespondencia #' . $request['n_control'] . ' creada satisfactoriamente!');
    }


    public function show($id)
    {


        $data = Correspondence::FindOrFail($id);

        $sel_typology = Typology::FindOrFail($data->tipology_id);
        $sel_direction = Direction::FindOrFail($data->direction_id);
        $sel_remitter = Remitter::FindOrFail($data->remitter_id);

        $tipologias = Typology::all();
        $direcciones = Direction::all();
        $remitentes = Remitter::all();

        $states = States::all()->pluck('name');
        // $state_id_actual = States::FindOrFail($request->state_id);

        // $data->state_name = $state_id_actual->name ;
        $show = true;

        //  return view('Admin.correspondencias.edit', compact('data'));
        return view('Admin.correspondencias.show')
            ->with('data', $data)
            ->with('sel_typology', $sel_typology)
            ->with('sel_direction', $sel_direction)
            ->with('sel_remitter', $sel_remitter)
            ->with('tipologias', $tipologias)
            ->with('direcciones', $direcciones)
            ->with('states', $states)
            ->with('remitentes', $remitentes)
            ->with('show', $show);
    }


    public function edit($id)
    {
        $data = Correspondence::FindOrFail($id);

        $sel_typology = Typology::FindOrFail($data->tipology_id);
        $sel_direction = Direction::FindOrFail($data->direction_id);
        $sel_remitter = Remitter::FindOrFail($data->remitter_id);

        $tipologias = Typology::all();
        $direcciones = Direction::all();
        $remitentes = Remitter::all();
        $states = States::pluck('name', 'id');
        $state_id_actual = States::FindOrFail($data->state_id);


        $show = false;



        //  return view('Admin.correspondencias.edit', compact('data'));
        return view('Admin.correspondencias.edit')
            ->with('data', $data)
            ->with('sel_typology', $sel_typology)
            ->with('sel_direction', $sel_direction)
            ->with('sel_remitter', $sel_remitter)
            ->with('tipologias', $tipologias)
            ->with('direcciones', $direcciones)
            ->with('remitentes', $remitentes)
            ->with('states', $states)
            ->with('state_id_actual', $data->state_id)
            ->with('show', $show);
    }

    public function attachPdfEmail($id, $author, $request)
    {

        $data = Correspondence::FindOrFail($id);

        $sel_typology = Typology::FindOrFail($data->tipology_id);
        $sel_remitter = Remitter::FindOrFail($data->remitter_id);
        $sel_direction = Direction::FindOrFail($data->direction_id);

        $data->remitter = $sel_remitter->name;
        $data->typology = $sel_typology->name;
        $data->direction = $sel_direction->name;
        // return response()->json($data);

        $viewRender = \view('Admin.correspondencias.template', ['data' => $data])->render();



        PDF::SetTitle('Correspondencia: ' . $data->n_control);


        PDF::SetAuthor('INPARQUES');
        PDF::SetTitle('Correspondencia: ' . $data->n_control);
        PDF::SetFont('dejavusans', '', 10);




        PDF::setFooterCallback(function ($pdf) {

            $pdf->SetFont('helvetica', 'I', 8);
            $pdf->SetY(-35);

            $data1 = 'Antes de imprimir este mensaje, asegúrese de que es necesario.';
            $pdf->Ln();
            $pdf->Cell(0,  0, $data1, 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $data2 =  '"Por cada árbol que se tala, por cada animal que muere,';
            $pdf->Ln();
            $pdf->Cell(0,  0, $data2, 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $data3 =   'por cada río, mar, océano que es contaminado,';
            $pdf->Ln();
            $pdf->Cell(0,  0, $data3, 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $data4 =   'el planeta sufre heridas profundas difíciles de cicatrizar".';
            $pdf->Ln();
            $pdf->Cell(0,  0, $data4, 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $data5 =   '¡Salvemos el Planeta!';
            $pdf->Ln();
            $pdf->Cell(0,  0, $data5, 0, false, 'C', 0, '', 0, false, 'M', 'M');


            $x = 20;
            $y = 280;
            $w = 169;
            $h = 12;

            $pdf->Image('img/footer.jpg', $x, $y, $w, $h, 'JPG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 0, false, false, false);
        });



        PDF::SetMargins(20, 20, 20, false);
        PDF::SetAutoPageBreak(true, 20);
        PDF::addPage();


        // PDF::writeHTML($viewRender, true, 0, true, 0);
        $pdf = PDF::writeHTML($viewRender, true, false, true, false, '');


        // $pdf =  PDF::output('Correspondencia: ' . $data->n_control . '.pdf');
        Mail::to($author->email)
            ->send(new UpdateCorrespondenceMailable($request))
            ->attach(public_path("/img/banner2.jpeg"));
    }

    public function update(Request  $request)
    {

        $editor = auth()->user();

        $id = $request->input('id');
        $data = Correspondence::FindOrFail($id);


        $data->n_control = $request->n_control;
        $data->n_comunc = $request->n_comunc;
        $data->f_recibo = $request->f_recibo;
        $data->h_recibo = $request->h_recibo;
        $data->f_comunc = $request->f_comunc;
        $data->prefijo = $request->prefijo;
        $data->archivar = $request->archivar;
        $data->f_archivd = $request->f_archivd;
        $data->tipology_id = $request->tipology_id;
        $data->direction_id = $request->direction_id;
        $data->remitter_id = $request->remitter_id;
        $data->asunto = $request->asunto;
        $data->accion = $request->accion;
        $data->ubicacion = $request->ubicacion;
        $data->observaciones = $request->observaciones;
        $data->state_id = $request->state_id;
        $data->history = $request->history;
        $data->updated_at = date('d-m-Y H:i:s', strtotime("now"));
        $data->updated_at = date('d-m-Y H:i:s', strtotime("now"));





        if ($request->hasFile('attachtment')) {


            $oldFile = $data->attachtmentPath . $data->attachtmentName;

            if (Storage::disk('public')->exists($oldFile)) {
                Storage::disk('public')->delete($oldFile);
            }


            $filePath = 'adjuntos/' . $request->n_control . "/";
            $newFile = $request->file('attachtment');
            $newFileName =  date('d-m-Y', strtotime("now")) . "_" . $newFile->hashName();
            $newFileLink = str_replace(' ', '_', $newFileName);




            $newFile->storeAs($filePath, $newFileLink, 'public');


            $data->attachtmentName = $newFileName;
            $data->attachtmentPath = $filePath;
        }





        // return response()->json($request);

        $data->update();


        // return  response()->json($user->id);



        // // envio email avisando sobre la actualizacion de la correspondencia a ambas gerencias
        $states = States::FindOrFail($request->state_id);

        // return  response()->json($states);


        $request->request->add(['title' => "Notificación de Correspondencia"]);
        $request->request->add(['description' => "Se ha actualizado la correspondencia con el N° de control"]);
        $request->request->add(['body_title' => "El estado de la correspondencia es: "]);
        $request->request->add(['state_name' => $states->name]);
        $request->request->add(['link' => route('Admin.correspondence.show', $request->id)]);
        $request->request->add(['editor' =>  $editor->name]);


        // return  response()->json($request);



        $author =  User::FindOrFail($request->author_id); //obtengo al usuario autor. 

        $creatorGerencia = Direction::FindOrFail($author->direction_id);
        $recipientGerencia = Direction::FindOrFail($request->direction_id);

        $this->attachPdfEmail($id, $author, $request);




        // Mail::to($author->email)->send(new UpdateCorrespondenceMailable($request));
        // Mail::to($creatorGerencia->email)->send(new UpdateCorrespondenceMailable($request));
        // Mail::to($recipientGerencia->email)->send(new UpdateCorrespondenceMailable($request));


        // $shantall =  User::where('id', '2')->first(); //me traigo a shantall
        // if ($creatorGerencia->id != $shantall->direction_id) {
        //     Mail::to($shantall->email)->send(new UpdateCorrespondenceMailable($request));
        // }

        // if ($request->author_id != $editor->id) {
        //     Mail::to($editor->email)->send(new UpdateCorrespondenceMailable($request));
        // }



        // return response()->json($shantall);











        $data = [
            'text' => ' Corrspnc. ',
            'n_control' => $request->n_control,
            'state' => 'Actlzd',
            'mail_id' => $request->input('id'),
        ];



        $shantall->notify(new CorrespondenceNotification($data)); //envio notificacion de edicion a shantall 

        $author->notify(new CorrespondenceNotification($data)); //envio notificacion de edicion de la correspondencia al autor de la misma



        $editor->notify(new CorrespondenceNotification($data)); //envio notificacion de edicion de la correspondencia al editor de la misma







        $Gerencia = Direction::FindOrFail($request->direction_id); //me traigo la gerencia destinataria        
        $user =  User::where('direction_id', $Gerencia->id)->first(); //me traigo al usuario asociado a la gerencia destinataria 
        if (auth()->user()->id != $user->id) {
            // return  response()->json($user->id);
            $user->notify(new CorrespondenceNotification($data));  //envio notificacion de edicion a la gerencia destinataria 
        }





        // return  response()->json($data); 
        return redirect()->route('Admin.correspondence.index')->with('info', 'Corrrespondencia actualizada satisfactoriamente!');
    }


    public function delete($id)
    {



        try {
            $data = Correspondence::query()->find($id);
            $data->delete();
        } catch (\Exception $e) {
            return redirect()->route('Admin.correspondence.index')->with('ERROR', $e->getMessage());
        }



        return redirect()->route('Admin.correspondence.index')->with('danger', 'Corrrespondencia eliminada satisfactoriamente!');
    }


    public function forceDelete($id)
    {

        if (empty($id)) {
            return redirect()->route('Admin.correspondence.index')->with('danger', 'Datos vacios, por favor contacte al personal de soporte!');
        } else {

            $data = Correspondence::withTrashed()->find($id);
            // dd($data);
            // return response()->json($data);

            if ($data != null) {
                $data->forceDelete();
                return redirect()->route('Admin.correspondence.index')->with('success', 'Correspondencia eliminada satisfactoriamente satisfactoriamente!');
            }

            return redirect()->route('Admin.correspondence.deleted')->with('warning', 'Correspondencia no encontrada!');
        }
    }


    public function restore($id)
    {
        $data = Correspondence::withTrashed()->find($id)->restore();
        return redirect()->route('Admin.correspondence.index')->with('info', 'Corrrespondencia restaurada satisfactoriamente!');
    }



    public function pdf($id)
    {

        $data = Correspondence::FindOrFail($id);

        $sel_typology = Typology::FindOrFail($data->tipology_id);
        $sel_remitter = Remitter::FindOrFail($data->remitter_id);
        $sel_direction = Direction::FindOrFail($data->direction_id);

        $data->remitter = $sel_remitter->name;
        $data->typology = $sel_typology->name;
        $data->direction = $sel_direction->name;
        // return response()->json($data);

        $viewRender = \view('Admin.correspondencias.template', ['data' => $data])->render();



        PDF::SetTitle('Correspondencia: ' . $data->n_control);


        PDF::SetAuthor('INPARQUES');
        PDF::SetTitle('Correspondencia: ' . $data->n_control);
        PDF::SetFont('dejavusans', '', 10);




        PDF::setFooterCallback(function ($pdf) {

            $pdf->SetFont('helvetica', 'I', 8);
            $pdf->SetY(-35);

            $data1 = 'Antes de imprimir este mensaje, asegúrese de que es necesario.';
            $pdf->Ln();
            $pdf->Cell(0,  0, $data1, 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $data2 =  '"Por cada árbol que se tala, por cada animal que muere,';
            $pdf->Ln();
            $pdf->Cell(0,  0, $data2, 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $data3 =   'por cada río, mar, océano que es contaminado,';
            $pdf->Ln();
            $pdf->Cell(0,  0, $data3, 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $data4 =   'el planeta sufre heridas profundas difíciles de cicatrizar".';
            $pdf->Ln();
            $pdf->Cell(0,  0, $data4, 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $data5 =   '¡Salvemos el Planeta!';
            $pdf->Ln();
            $pdf->Cell(0,  0, $data5, 0, false, 'C', 0, '', 0, false, 'M', 'M');


            $x = 20;
            $y = 280;
            $w = 169;
            $h = 12;

            $pdf->Image('img/footer.jpg', $x, $y, $w, $h, 'JPG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 0, false, false, false);
        });




        PDF::SetMargins(20, 20, 20, false);
        PDF::SetAutoPageBreak(true, 20);
        PDF::addPage();


        PDF::writeHTML($viewRender, true, 0, true, 0);
        PDF::output('Correspondencia: ' . $data->n_control . '.pdf');

        // return view('Admin.correspondencias.tcpdf' )->with('data',$data);

    }

    public function attachs($id)
    {

        $data = Correspondence::FindOrFail($id);


        $fileExtension =  File::extension($data->attachtmentName);
        $file = $data->attachtmentPath . $data->attachtmentName;

        if (Storage::disk('public')->exists($file)) {
            $file2 = Storage::disk('public')->get($file);
            return (new Response($file2, 200))->header('Content-Type', $fileExtension);
        }

        return redirect()->route('Admin.correspondence.index')->with('success', 'Documento no encontrado. Por favor contacte al personal de soporte.');
    }



    public function deleted()
    {
        return view('Admin.correspondencias.deleted');
    }
}