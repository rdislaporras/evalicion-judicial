<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

use Auth;

class ProfileController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {

        $data = Auth::user();


        if (Auth::user()->avatar == null) {
            $data->avatar = Auth::user()->adminlte_image();
        }


        return view('Admin.profile.show')->with('data', Auth::user());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {



        return view('Admin.profile.edit')->with('data', Auth::user());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $User)
    {

        // return response()->json($path);

        $path = Storage::disk('public')->putFileAs('storage/images/avatars', $request->file('avatar'), 'photo_' . date('d-m-Y H:i:s', strtotime("now")) . '.jpg');

        $user = Auth::user();
        $user->name         = $request['name'];
        $user->email        = $request['email'];
        $user->direction_id = $request['direction_id'];
        $user->password     = bcrypt($request['password']);
        $user->avatar       = $path;
        $user->save();

        return redirect()->route('Admin.profile.show', Auth::id())
            ->with('succees', 'Usuario actualizado satisfactoriamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}