<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Admin\Direction;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

class UserController extends Controller
{



    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('Admin.users.index');
    }

    public function create()
    {

        $direcciones = Direction::pluck('name', 'id');
        return view('Admin.users.create', ['direcciones' => $direcciones]);
    }


    public function store(Request $request)
    {

        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'email_verified_at' => now(),
            'password' => bcrypt($request['password']),
            'direction_id' => $request['direction_id'],
        ]);

        return redirect()->route('Admin.user.index')->with('succees', 'Usuario Creado satisfactoriamente!');
    }





    public function show($id)
    {
        $data = User::FindOrFail($id);

        // $sel_Role = Role::FindOrFail($data->role_id);
        // $sel_Permission = Permission::FindOrFail($data->remitter_id);

        $roles = Role::all();
        // $permisos = Permission::all();
        $direcciones = Direction::pluck('name', 'id');


        return view('Admin.users.show')
            ->with('data', $data)
            ->with('roles', $roles)
            ->with('direcciones', $direcciones);
    }


    public function edit($id)
    {
        $data = User::FindOrFail($id);

        // $sel_Role = Role::FindOrFail($data->role_id);
        // $sel_Permission = Permission::FindOrFail($data->remitter_id);

        $roles = Role::all();
        // $permisos = Permission::all();
        $direcciones = Direction::pluck('name', 'id');


        return view('Admin.users.edit')
            ->with('data', $data)
            ->with('roles', $roles)
            ->with('direcciones', $direcciones);
    }


    public function update(Request $request, User $user)
    {

        $user = User::find($request->id);


        $user->name         = $request['name'];
        $user->email        = $request['email'];
        $user->direction_id = $request['direction_id'];
        $user->password     = bcrypt($request['password']);
        $user->save();

        $user->roles()->sync($request->roles);
        return redirect()->route('Admin.user.index')->with('success', 'Usuario actualizado satisfactoriamente!');
    }



    public function delete($id)
    {



        try {
            $data = User::query()->find($id);
            $data->delete();
        } catch (\Exception $e) {
            return redirect()->route('Admin.user.index')->with('ERROR', $e->getMessage());
        }



        return redirect()->route('Admin.user.index')->with('danger', 'Usuario eliminada satisfactoriamente!');
    }


    public function forceDelete($id)
    {

        $data = User::query()->find($id);
        $data->forceDelete();
        return redirect()->route('Admin.user.index')->with('danger', 'Usuario eliminada satisfactoriamente!');
    }


    public function restore($id)
    {
        $data = User::withTrashed()->find($id)->restore();
        return redirect()->route('Admin.user.index')->with('info', 'Usuario restaurada satisfactoriamente!');
    }
}