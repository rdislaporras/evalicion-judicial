<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Products;
use App\Models\Admin\Purchases;
use App\Models\Admin\Invoices;
use Auth;

class ProductsController extends Controller
{


    public function index()
    {
        return view('Admin.products.index');
    }



    public function store(Request $request)
    {
        // Products::create($request->validated()); 

        // dd($request);
        $data = $request->except('_token');

        Products::insert($data);
        return redirect()->route('Admin.products.index');
    }


    // public function show(Productss $Productss)
    // {
    //     Products::create($request->validated());
    //     return redirect()->route('Admin.products.index');
    // }




    public function edit($id)
    {
        $product = Products::find($id);
        if ($product == null) {
            return redirect()->route('Admin.products.index')->with('info', 'Debe activar registro para poder editar');
        } else {
            return view('Admin.products.edit', compact('product'));
        }
    }


    public function update(Request $request)
    {
        $id = $request->input('id');

        $Products = Products::find($id);
        $Products->name = $request->name;
        $Products->price = $request->price;
        $Products->tax = $request->tax;
        $Products->quant = $request->quant;
        $Products->save();

        return redirect()->route('Admin.products.index')->with('success', 'Producto actualizado satisfactoriamente!');
    }


    // public function destroy($id)
    // {
    //     $Products = Products::find($id);
    //     $Products->delete();
    //     return redirect()->route('Admin.products.index');
    // }


    public function delete($id)
    {
        $data = Products::find($id);

        if ($data != null) {
            $data->delete();
            return redirect()->route('Admin.products.index')->with('success', 'Gerencia eliminada satisfactoriamente satisfactoriamente!');
        }

        return redirect()->route('Admin.products.index')->with('warning', 'Gerencia no encontrada!');
    }

    // public function trashed()
    // {
    //     return view('Admin.products.trashed');
    // }




    // public function restore($id)
    // {
    //     $direcciones = Products::withTrashed()->find($id);
    //     if ($direcciones->deleted_at != null) {
    //         $direcciones->restore();
    //         return redirect()->route('Admin.products.trashed')
    //             ->with('info', 'Gerencia restaurada');
    //     } else {
    //         return redirect()->route('Admin.products.trashed')
    //             ->with('info', 'Gerencia no encontrada ');
    //     }
    // }


    // public function forceDelete($id)
    // {

    //     Products::withTrashed()->find($id)->forceDelete();
    //     return redirect()->route('Admin.products.trashed');
    // }




    public function buy($id)
    {
        $product = Products::find($id);
        if ($product == null) {
            return redirect()->route('Admin.products.index')->with('info', 'Debe activar registro para poder editar');
        } else {
            return view('Admin.products.buy', compact('product'));
        }
    }

    public function purchase($id)
    {
        $product = Products::find($id);
        // $data =  new \stdClass();



        if ($product->quant > 0) {

            $product->quant = $product->quant - 1;
            $product->save();


            $purchase = Purchases::create(
                ['user_id' => Auth::user()->id, 'product_id' => $product->id]
            );

            Invoices::create([
                'purchase_id' => $purchase->id,
                // 'pending' => false,
            ]);
        } else {
            return redirect('/')->with('warning', 'producto no disponible');
        };

        return redirect('/')->with('info', 'Compra realizada satisfactoriamente');
    }
}