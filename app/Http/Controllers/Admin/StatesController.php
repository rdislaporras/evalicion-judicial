<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\States;


class StatesController extends Controller
{


    public function index()
    {
        return view('Admin.states.index');
    }


    public function store(Request $request)
    {
        $data = $request->except('_token');
        States::insert($data);

        // return response()->json($data);
        return redirect()->route('Admin.states.index')->with('success', 'Se creo un estado llamado: ' . $data['name'] . ' satisfactoriamente!');
    }

    public function edit($id)
    {
        $data = States::FindOrFail($id);

        return view('Admin.states.edit')->with('data', $data);
    }


    public function update(Request $request, States $states)
    {
        $id = $request->input('id');
        $data = States::FindOrFail($id);

        $data->name = $request->name;

        $data->update();
        // return response()->json($data);
        return redirect()->route('Admin.states.index')->with('info', 'Estado actualizado
        satisfactoriamente!');
    }


    public function delete($id)
    {
        $data = States::find($id);

        if ($data != null) {
            $data->delete();
            return redirect()->route('Admin.states.index')->with('success', 'Estado eliminado satisfactoriamente');
        }

        return redirect()->route('Admin.states.index')->with('danger', 'Valide el Estado a eliminar');
    }


    public function trashed()
    {
        return view('Admin.states.trashed');
    }


    public function forceDelete($id)
    {
        States::withTrashed()->find($id)->forceDelete();
        return redirect()->route('Admin.states.trashed')->with('success', 'Estado eliminado satisfactoriamente!');
    }


    public function restore($id)
    {
        $data = States::withTrashed()->find($id)->restore();
        return redirect()->route('Admin.correspondence.index')->with('info', 'Estado restaurado
        satisfactoriamente!');
    }
}