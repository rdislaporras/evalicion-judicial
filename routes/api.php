<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Admin\Correspondence;
use App\Models\Admin\Products;
use App\Models\Admin\Remitter;
use App\Models\Admin\Typology;
use App\Models\User;
use App\Models\Admin\States;
use App\Models\Admin\Correspondencia;
use App\Models\Admin\Notifications;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('usuarios', function () {
    return datatables()
        ->eloquent(User::orderBy('id', 'DESC'))
        ->addColumn('btn', 'Admin/users/partials/options')
        ->rawColumns(['btn'])
        ->toJson();
});



Route::get('products', function () {
    return datatables()
        ->eloquent(Products::orderBy('id', 'DESC'))
        ->addColumn('options', 'Admin/products/partials/options')
        ->rawColumns(['options'])
        ->toJson();
});

Route::get('products.buy', function () {
    return datatables()
        ->eloquent(Products::orderBy('id', 'DESC'))
        ->addColumn('buy', 'Admin/products/partials/buy')
        ->rawColumns(['buy'])
        ->toJson();
});



// Route::get('correspondence.deleted', function () {
//     return datatables()
//         ->eloquent(Correspondence::onlyTrashed())
//         ->addColumn('btn', 'Admin/correspondencias/partials/options')
//         ->rawColumns(['btn'])
//         ->toJson();
// });




// Route::get('typologies.trashed  ', function () {
//     return datatables()
//         ->eloquent(Typology::onlyTrashed())
//         ->addColumn('btn', 'Admin.typologies.partials.options')
//         ->rawColumns(['btn'])
//         ->toJson();
// });


// Route::get('estados', function () {
//     return datatables()
//         ->eloquent(States::orderBy('id', 'DESC'))
//         ->addColumn('btn', 'Admin/states/partials/options')
//         ->rawColumns(['btn'])
//         ->toJson();
// });

// Route::get('estados-trashed', function () {
//     return datatables()
//         ->eloquent(States::onlyTrashed())
//         ->addColumn('btn', 'Admin/states/partials/options')
//         ->rawColumns(['btn'])
//         ->toJson();
// });





// Route::get('notificaciones', function () {
//     return datatables()
//         ->eloquent(Notifications::orderBy('id', 'DESC'))
//         ->addColumn('options', 'Admin/notifications/partials/options')
//         ->rawColumns(['options'])
//         ->toJson();
// });