<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ProductsController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\StatesController;
use App\Http\Controllers\Admin\InvoicesController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('/welcome');
    return redirect()->route('login');
});




Auth::routes();

Route::post('/registrar-usuario', 'RegisterController@auth\register');


Route::group(['middelware' => ['auth']], function () {
    Route::get('escritorio',            [App\Http\Controllers\Admin\HomeController::class,              'index'])->name('Admin.home.index');



    // crud de perfil
    Route::get('perfil/editar/{slug}',   [ProfileController::class, 'edit'])->name('Admin.profile.edit');
    Route::get('perfil/detalles/{slug}', [ProfileController::class, 'show'])->name('Admin.profile.show');
    Route::post('perfil/actualizar',     [ProfileController::class, 'update'])->name('Admin.profile.update');


    /**
     * Grupo de rutas crud de usuarios
     */
    Route::controller(UserController::class)->group(function () {
        Route::get('usuarios',                  'index')->middleware('can:Admin.user.index')->name('Admin.user.index');
        Route::get('usuarios/todas',            'all')->middleware('can:Admin.user.all')->name('Admin.user.all');
        Route::get('usuarios/nueva',            'create')->middleware('can:Admin.user.create')->name('Admin.user.create');
        Route::post('usuarios',                 'store')->middleware('can:Admin.user.store')->name('Admin.user.store');
        Route::get('usuarios/mostrar/{slug}',   'show')->middleware('can:Admin.user.show')->name('Admin.user.show');
        Route::get('usuarios/editar/{slug}',    'edit')->middleware('can:Admin.user.edit')->name('Admin.user.edit');
        Route::PUT('usuarios',                  'update')->middleware('can:Admin.user.update')->name('Admin.user.update');
        Route::get('usuarios/delete/{slug}',    'delete')->middleware('can:Admin.user.delete')->name('Admin.user.delete');
        Route::get('usuarios/{slug}',           'forceDelete')->middleware('can:Admin.user.forceDelete')->name('Admin.user.forceDelete');
        Route::get('usuarios/restore/{slug}',   'restore')->middleware('can:Admin.user.restore')->name('Admin.user.restore');
    });




    /**
     * Grupo de rutas crud de correspondencias
     */
    Route::controller(ProductsController::class)->group(function () {
        Route::get('Productos',                  'index')->name('Admin.products.index');
        Route::get('Producto/comprar/{slug}',    'buy')->name('Admin.products.buy');
        Route::patch('Producto/comprado/{slug}',  'purchase')->name('Admin.products.purchase');
        Route::get('Productos-directions',       'MailxDir')->name('Admin.products.MailxDir');
        Route::get('Productos-eliminadas',       'deleted')->middleware('can:Admin.products.deleted')->name('Admin.products.deleted');
        Route::get('Productos/todas',            'all')->name('Admin.products.all');
        Route::get('Productos/crear',            'create')->middleware('can:Admin.products.create')->name('Admin.products.create');
        Route::post('Productos',                 'store')->name('Admin.products.store');
        Route::get('Productos/mostrar/{slug}',   'show')->name('Admin.products.show');
        Route::get('Productos/editar/{slug}',    'edit')->name('Admin.products.edit');
        Route::patch('Productos',                  'update')->name('Admin.products.update');
        Route::get('Productos/delete/{slug}',    'delete')->name('Admin.products.delete');
        Route::get('Productos/{slug}',           'forceDelete')->name('Admin.products.forceDelete');
        Route::get('Productos/restore/{slug}',   'restore')->name('Admin.products.restore');
        Route::get('Productos/pdf/{slug}',        'pdf')->name('Admin.products.pdf');
        Route::get('Productos/adjuntos/{slug}',   'attachs')->name('Admin.products.attachs');
    });




    /**
     * Grupo de rutas crud de correspondencias
     */
    Route::controller(StatesController::class)->group(function () {

        Route::get('estados',                   'index')->middleware('can:Admin.states.create')->name('Admin.states.index');
        Route::get('estados-eliminados',        'trashed')->name('Admin.states.trashed');
        Route::get('estados/todos',             'all')->name('Admin.states.all');
        Route::post('estados',                  'store')->middleware('can:Admin.states.create')->name('Admin.states.store');
        Route::get('estados/mostrar/{slug}',    'show')->name('Admin.states.show');
        Route::get('estados/editar/{slug}',     'edit')->middleware('can:Admin.states.create')->name('Admin.states.edit');
        Route::PUT('estados',                   'update')->name('Admin.states.update');
        Route::get('estados/delete/{slug}',     'delete')->name('Admin.states.delete');
        Route::get('estados/{slug}',            'forceDelete')->name('Admin.states.forceDelete');
        Route::get('estados/restore/{slug}',    'restore')->name('Admin.states.restore');
    });


    Route::controller(NotificationsController::class)->group(function () {
        Route::get('notificacions',                 'index')->name('Admin.notifications.index');
        Route::get('notifications/get',             'getNotificationsData')->name('Admin.notifications.get');
        Route::get('notifications/notify',          'notify')->name('Admin.notifications.notify');
        Route::get('notifications/id/{slug}/mail/{slug2}',   'check')->name('Admin.notifications.check');
        Route::post('notifications/markAsRead',     'markAsRead')->name('Admin.notifications.markAsRead');
        Route::post('notifications/delete',         'delete')->name('Admin.notifications.delete');
    });


    Route::controller(InvoicesController::class)->group(function () {
        Route::get('notificacions',                 'index')->name('Admin.invoices.index');
        Route::get('invoices/get',                  'get')->name('Admin.invoices.get');
        Route::get('invoices/mostrar/{slug}',      'show')->name('Admin.invoices.show');
        Route::post('invoices/markAsRead',          'markDone')->name('Admin.invoices.markDone');
        // Route::post('invoices/delete',         'delete')->name('Admin.notifications.delete');
    });
});