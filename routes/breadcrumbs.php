<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('Admin.home.index'));
});





// CORRESPONDENCIAS
// Home > correspondencias
Breadcrumbs::for('correspondencias', function ($trail) {
    $trail->parent('home');
    $trail->push('Correspondencias 2022', route('Admin.correspondence.index'));
});

// Home > correspondencias
Breadcrumbs::for('correspondencias/todas', function ($trail) {
    $trail->parent('home');
    $trail->push('Correspondencias borradas', route('Admin.correspondence.all'));
});

// Home > correspondencias > nueva
Breadcrumbs::for('Admin.correspondencia.create', function ($trail) {
    $trail->parent('correspondencias');
    $trail->push('Nueva', route('Admin.correspondence.create'));
});

// Home > correspondencias > editar
Breadcrumbs::for('Admin.correspondencia.edit', function ($trail, $id) {
    $trail->parent('correspondencias');
    $trail->push('Editar Correspondencia: ' . $id, route('Admin.correspondence.edit', $id));
});


// Home > correspondencias > mostrar
Breadcrumbs::for('Admin.correspondencia.show', function ($trail, $id) {
    $trail->parent('correspondencias');
    $trail->push('Mostrar Correspondencia: ' . $id, route('Admin.correspondence.show', $id));
});



// Home > correspondencias antiguas
Breadcrumbs::for('correspondencias2020', function ($trail) {
    $trail->parent('home');
    $trail->push('Correspondencias antiguas', route('Admin.2020.index'));
});


// Home > correspondencias > crear
Breadcrumbs::for('Admin.remitters.create', function ($trail) {
    $trail->parent('home');
    $trail->push('Nuevo', route('Admin.remitters.create'));
});



// END CORRESPONDENCIAS



// TIPOLOGIAS
// Home > tipologias
Breadcrumbs::for('tipologias', function ($trail) {
    $trail->parent('home');
    $trail->push('Tipologías', route('Admin.typologies.index'));
});

// Home > direcciones > editar
Breadcrumbs::for('Admin.typologies.edit', function ($trail, $id) {
    $trail->parent('tipologias');
    $trail->push('Editar Tipo: ' . $id, route('Admin.typologies.edit', $id));
});




// Home > tipologias ELIMINADAS
Breadcrumbs::for('tipologias-eliminadas', function ($trail) {
    $trail->parent('home');
    $trail->push('Tipologías eliminadas', route('Admin.typologies.trashed'));
});

// END  TIPOLOGIAS


// REMITENTES
// Home > remitentes
Breadcrumbs::for('remitentes', function ($trail) {
    $trail->parent('home');
    $trail->push('Remitentes', route('Admin.remitters.index'));
});

// Home > remitentes eliminados
Breadcrumbs::for('remitentes-eliminados', function ($trail) {
    $trail->parent('home');
    $trail->push('Remitentes eliminados', route('Admin.remitters.trashed'));
});

// END REMITENTES



// DIRECCIONES
// Home > direcciones
Breadcrumbs::for('gerencias', function ($trail) {
    $trail->parent('home');
    $trail->push('Gerencias', route('Admin.directions.index'));
});
Breadcrumbs::for('Admin.directions.trashed', function ($trail) {
    $trail->parent('home');
    $trail->push('Gerencias Eliminadas', route('Admin.directions.trashed'));
});

// Home > direcciones > editar
Breadcrumbs::for('Admin.directions.edit', function ($trail, $id) {
    $trail->parent('gerencias');
    $trail->push('Editar Gerencia: ' . $id, route('Admin.directions.edit', $id));
});



// END DIRECCIONES



// PERFILES


// Home > profile > mostrar
Breadcrumbs::for('Admin.profile.show', function ($trail, $id) {
    $trail->parent('home');
    $trail->push('Detalles del usuario: ' . $id, route('Admin.profile.show', $id));
});


// Home > profile > editar
Breadcrumbs::for('Admin.profile.edit', function ($trail, $id) {
    $trail->parent('home');
    $trail->push('Editar usuario: ' . $id, route('Admin.profile.edit', $id));
});

// END PERFILES

// USUARIOS
// Home > usuarios
Breadcrumbs::for('usuarios', function ($trail) {
    $trail->parent('home');
    $trail->push('Usuarios', route('Admin.user.index'));
});


// Home > usuarios > crear
Breadcrumbs::for('Admin.user.create', function ($trail) {
    $trail->parent('usuarios');
    $trail->push('Crear Usuario ', route('Admin.user.create'));
});

// Home > usuarios > mostrar
Breadcrumbs::for('Admin.user.show', function ($trail, $id) {
    $trail->parent('usuarios');
    $trail->push('Mostrar Usuario ', route('Admin.user.show', $id));
});


// Home > usuarios > editar
Breadcrumbs::for('Admin.user.edit', function ($trail, $id) {
    $trail->parent('usuarios');
    $trail->push('Editar Usuario: ' . $id, route('Admin.user.edit', $id));
});

// END USUARIOS



// STADOS
// Home > states
Breadcrumbs::for('states', function ($trail) {
    $trail->parent('home');
    $trail->push('Estados', route('Admin.states.index'));
});


// Home > states > new
Breadcrumbs::for('Admin.states.create', function ($trail) {
    $trail->parent('home');
    $trail->push('Nuevo Estado', route('Admin.states.create'));
});


// Home > states > editar
Breadcrumbs::for('Admin.states.edit', function ($trail, $id) {
    $trail->parent('states');
    $trail->push('Editar estado: ' . $id, route('Admin.states.edit', $id));
});


// Home > states > mostrar
Breadcrumbs::for('Admin.states.show', function ($trail, $id) {
    $trail->parent('states');
    $trail->push('Mostrar estado: ' . $id, route('Admin.states.show', $id));
});

// Home > states eliminados
Breadcrumbs::for('states-eliminados', function ($trail) {
    $trail->parent('home');
    $trail->push('Estados eliminados', route('Admin.states.trashed'));
});


// END  STADOS




// Home > notificaciones
Breadcrumbs::for('notifications', function ($trail) {
    $trail->parent('home');
    $trail->push('Notificaciones', route('Admin.notifications.index'));
});